/*
Navicat MySQL Data Transfer

Source Server         : 本地数据库
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : oa

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2015-07-21 17:54:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `oa_building`
-- ----------------------------
DROP TABLE IF EXISTS `oa_building`;
CREATE TABLE `oa_building` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL COMMENT '父级ID',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '名称',
  `sort` varchar(20) NOT NULL COMMENT '排序',
  `remark` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_del` tinyint(3) NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `acreage` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '面积',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of oa_building
-- ----------------------------
INSERT INTO `oa_building` VALUES ('1', '0', '#s1', '1', '', '0', '平房区', '10000平方米');
INSERT INTO `oa_building` VALUES ('2', '0', '#s2', '2', '', '0', '平方', '20000');
INSERT INTO `oa_building` VALUES ('3', '1', '1层', '1', '', '0', '平凡区', '100');
INSERT INTO `oa_building` VALUES ('4', '0', '#s3', '4', '', '0', '南岗区', '100000');
INSERT INTO `oa_building` VALUES ('5', '1', '2层', '2', '', '0', '1111', '1111');
INSERT INTO `oa_building` VALUES ('6', '1', '3层', '3', '', '0', '111', '111');
INSERT INTO `oa_building` VALUES ('8', '2', '1层', '1', '', '0', '11', '11');
INSERT INTO `oa_building` VALUES ('9', '4', '1层', '', '', '0', '11', '111');
INSERT INTO `oa_building` VALUES ('10', '2', '2层', '2', '', '0', '1', '1');

-- ----------------------------
-- Table structure for `oa_contact`
-- ----------------------------
DROP TABLE IF EXISTS `oa_contact`;
CREATE TABLE `oa_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `letter` varchar(50) NOT NULL DEFAULT '' COMMENT '拼音',
  `company` varchar(30) NOT NULL DEFAULT '' COMMENT '公司',
  `dept` varchar(20) NOT NULL DEFAULT '' COMMENT '部门',
  `position` varchar(20) NOT NULL DEFAULT '' COMMENT '职位',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '邮件',
  `office_tel` varchar(20) NOT NULL DEFAULT '' COMMENT '办公电话',
  `mobile_tel` varchar(20) NOT NULL DEFAULT '' COMMENT '移动电话',
  `website` varchar(50) NOT NULL DEFAULT '' COMMENT '网站',
  `im` varchar(20) NOT NULL DEFAULT '' COMMENT '即时通讯',
  `address` varchar(50) NOT NULL DEFAULT '' COMMENT '地址',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `remark` text COMMENT '备注',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='think_user_info';

-- ----------------------------
-- Records of oa_contact
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_customer`
-- ----------------------------
DROP TABLE IF EXISTS `oa_customer`;
CREATE TABLE `oa_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
  `letter` varchar(50) NOT NULL DEFAULT '' COMMENT '拼音',
  `biz_license` varchar(30) NOT NULL DEFAULT '' COMMENT '营业许可',
  `short` varchar(20) NOT NULL DEFAULT '' COMMENT '简称',
  `contact` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人姓名',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '邮件地址',
  `office_tel` varchar(20) NOT NULL DEFAULT '' COMMENT '办公电话',
  `mobile_tel` varchar(20) NOT NULL DEFAULT '' COMMENT '移动电话',
  `fax` varchar(20) NOT NULL DEFAULT '' COMMENT '传真',
  `salesman` varchar(50) NOT NULL DEFAULT '' COMMENT '业务员',
  `im` varchar(20) NOT NULL DEFAULT '' COMMENT '即时通讯',
  `address` varchar(50) NOT NULL DEFAULT '' COMMENT '地址',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `remark` text COMMENT '备注',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `payment` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '状态   0 初步意向 1 比较有意向 2密切接触',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_customer
-- ----------------------------
INSERT INTO `oa_customer` VALUES ('3', '测试的公司名称阿斯顿发送到阿斯顿发', 'CSDGSMC', '11111111111', '测试的', '小米', '576216559@qq.com', '15104613361', '15104613361', '15104613361', '小李', '', '大直街', '0', '', '0', '576216559@qq.com', '1');
INSERT INTO `oa_customer` VALUES ('4', '测试招商企业', 'CSZSQY', '测试招商企业', '测试招商企业', '测试招商企业', '测试招商企业', '18845727259', '15104613361', '18845727259', '测试招商企业', '测试招商企业', '测试招商企业', '0', '测试招商企业', '0', '111', '1');
INSERT INTO `oa_customer` VALUES ('5', '我是新添加公司', 'WSXTJGS', '我是新添加公司', '我是新添加公司', '18845727259', '我是新添加公司', '我是新添加公司', '我是新添加公司', '我是新添加公司', '我是新添加公司', '我是新添加公司', '我是新添加公司', '0', '我是新添加公司', '0', '我是新添加公司', null);

-- ----------------------------
-- Table structure for `oa_dept`
-- ----------------------------
DROP TABLE IF EXISTS `oa_dept`;
CREATE TABLE `oa_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `dept_no` varchar(20) NOT NULL DEFAULT '' COMMENT '部门编号',
  `dept_grade_id` int(11) NOT NULL DEFAULT '0' COMMENT '部门等级ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `short` varchar(20) NOT NULL DEFAULT '' COMMENT '简称',
  `sort` varchar(20) NOT NULL DEFAULT '' COMMENT '排序',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_dept
-- ----------------------------
INSERT INTO `oa_dept` VALUES ('1', '0', 'A2', '18', '小微企业', '小微', '', '', '0');
INSERT INTO `oa_dept` VALUES ('2', '1', 'YYB', '18', '运营部', '运营', '5', '', '0');
INSERT INTO `oa_dept` VALUES ('3', '1', 'XXB', '18', 'IT部', 'IT', '4', '', '0');
INSERT INTO `oa_dept` VALUES ('5', '1', 'ZJB', '18', '总经办', '总经', '1', '', '0');
INSERT INTO `oa_dept` VALUES ('6', '1', 'GLB', '18', '管理部', '管理', '2', '', '0');
INSERT INTO `oa_dept` VALUES ('7', '1', 'XSB', '18', '销售部', '销售', '3', '', '0');
INSERT INTO `oa_dept` VALUES ('8', '1', 'CWB', '18', '财务部', '财务', '2', '', '0');
INSERT INTO `oa_dept` VALUES ('21', '1', 'XSB', '18', '采购部', '采购', '3', '', '0');
INSERT INTO `oa_dept` VALUES ('23', '6', 'HR', '16', '人事科', '人事', '', '', '0');
INSERT INTO `oa_dept` VALUES ('24', '6', 'ZWK', '16', '总务科', '总务', '', '', '0');
INSERT INTO `oa_dept` VALUES ('25', '8', 'KJK', '16', '会计科', '会计', '', '', '0');
INSERT INTO `oa_dept` VALUES ('26', '8', 'JRK', '16', '金融科', '金融', '', '', '0');

-- ----------------------------
-- Table structure for `oa_dept_grade`
-- ----------------------------
DROP TABLE IF EXISTS `oa_dept_grade`;
CREATE TABLE `oa_dept_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_no` varchar(10) NOT NULL DEFAULT '' COMMENT '部门级别编码',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `sort` varchar(10) NOT NULL DEFAULT '' COMMENT '排序',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_dept_grade
-- ----------------------------
INSERT INTO `oa_dept_grade` VALUES ('16', 'DG1', '科', '1', '0');
INSERT INTO `oa_dept_grade` VALUES ('18', 'DG2', '部', '2', '0');

-- ----------------------------
-- Table structure for `oa_doc`
-- ----------------------------
DROP TABLE IF EXISTS `oa_doc`;
CREATE TABLE `oa_doc` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(20) NOT NULL DEFAULT '' COMMENT '文档编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `content` text NOT NULL COMMENT '内容',
  `folder` int(11) NOT NULL DEFAULT '0' COMMENT '文件夹',
  `add_file` varchar(200) NOT NULL DEFAULT '' COMMENT '附件',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名称',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `name_type` int(1) NOT NULL DEFAULT '0' COMMENT '文件收发类型  0 收件 1发件',
  `keyword` varchar(200) NOT NULL COMMENT '关键词',
  `file_type` int(1) NOT NULL DEFAULT '0' COMMENT '是否归档 0  不归档 1归档',
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(20) NOT NULL,
  `gd_name` varchar(20) NOT NULL COMMENT '归档部门',
  `gd_id` int(11) NOT NULL COMMENT '归档id',
  `scope_user_id` text,
  `scope_user_name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_doc
-- ----------------------------
INSERT INTO `oa_doc` VALUES ('28', '2015-0017', '4444444444', '4444444444444444444', '5', '', '1', '管理员', '1437116992', '1437464595', '0', '1', '4444444444', '1', '1', '小微企业', '总经办', '5', ',', '销售部|;');

-- ----------------------------
-- Table structure for `oa_duty`
-- ----------------------------
DROP TABLE IF EXISTS `oa_duty`;
CREATE TABLE `oa_duty` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `duty_no` varchar(20) NOT NULL DEFAULT '' COMMENT '职责编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `sort` varchar(20) NOT NULL DEFAULT '' COMMENT '排序',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_duty
-- ----------------------------
INSERT INTO `oa_duty` VALUES ('14', 'P001', '普通员工', '', '0', '');
INSERT INTO `oa_duty` VALUES ('15', 'S001', '财务', '', '0', '');
INSERT INTO `oa_duty` VALUES ('16', 'W001', '人事', '', '0', '');

-- ----------------------------
-- Table structure for `oa_enter`
-- ----------------------------
DROP TABLE IF EXISTS `oa_enter`;
CREATE TABLE `oa_enter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) CHARACTER SET utf8 NOT NULL,
  `telephone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `email` varchar(30) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_del` tinyint(3) NOT NULL,
  `sort` varchar(20) NOT NULL,
  `room_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '审核 0待审核 1已审核',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of oa_enter
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_file`
-- ----------------------------
DROP TABLE IF EXISTS `oa_file`;
CREATE TABLE `oa_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(20) NOT NULL DEFAULT '' COMMENT '保存名称',
  `savepath` char(30) NOT NULL DEFAULT '' COMMENT '文件保存路径',
  `ext` char(5) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` char(40) NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '远程地址',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='文件表';

-- ----------------------------
-- Records of oa_file
-- ----------------------------
INSERT INTO `oa_file` VALUES ('1', '新媒体.txt', '55a31fd1cda03.txt', 'mail/2015-07/', 'txt', 'text/plain', '141', 'cedd577e6dd59b6e0cd73d0793a5b0eb', '57c78d8d64ebd2f6f446ba6e6ad8d6791f6b79e2', '0', '', '1436753873');
INSERT INTO `oa_file` VALUES ('2', '新媒体.txt', '55a32023ee82d.txt', 'mail/2015-07/', 'txt', 'text/plain', '141', 'cedd577e6dd59b6e0cd73d0793a5b0eb', '57c78d8d64ebd2f6f446ba6e6ad8d6791f6b79e2', '0', '', '1436753955');
INSERT INTO `oa_file` VALUES ('3', '新媒体.txt', '55a32db3a5e57.txt', 'mail/2015-07/', 'txt', 'text/plain', '141', 'cedd577e6dd59b6e0cd73d0793a5b0eb', '57c78d8d64ebd2f6f446ba6e6ad8d6791f6b79e2', '0', '', '1436757427');
INSERT INTO `oa_file` VALUES ('4', '新媒体.txt', '55a32ded2ba6f.txt', 'mail/2015-07/', 'txt', 'text/plain', '141', 'cedd577e6dd59b6e0cd73d0793a5b0eb', '57c78d8d64ebd2f6f446ba6e6ad8d6791f6b79e2', '0', '', '1436757484');
INSERT INTO `oa_file` VALUES ('5', '新媒体.txt', '55a32ff9546b4.txt', 'doc/2015-07/', 'txt', 'text/plain', '141', 'cedd577e6dd59b6e0cd73d0793a5b0eb', '57c78d8d64ebd2f6f446ba6e6ad8d6791f6b79e2', '0', '', '1436758009');
INSERT INTO `oa_file` VALUES ('6', '新媒体.txt', '55a33bb7efaf6.txt', 'info/2015-07/', 'txt', 'text/plain', '195', '8ef899adde6afee9e3541fd6e04c1c47', '3fa40ad3dbad064417cf7dd7526f093fc6f799ee', '0', '', '1436761015');
INSERT INTO `oa_file` VALUES ('7', '新媒体.txt', '55a36088eb4f4.txt', 'doc/2015-07/', 'txt', 'text/plain', '195', '8ef899adde6afee9e3541fd6e04c1c47', '3fa40ad3dbad064417cf7dd7526f093fc6f799ee', '0', '', '1436770440');
INSERT INTO `oa_file` VALUES ('8', '新媒体.txt', '55a360c9d311c.txt', 'doc/2015-07/', 'txt', 'text/plain', '195', '8ef899adde6afee9e3541fd6e04c1c47', '3fa40ad3dbad064417cf7dd7526f093fc6f799ee', '0', '', '1436770505');
INSERT INTO `oa_file` VALUES ('9', '新媒体.txt', '55a36b39acf09.txt', 'doc/2015-07/', 'txt', 'text/plain', '195', '8ef899adde6afee9e3541fd6e04c1c47', '3fa40ad3dbad064417cf7dd7526f093fc6f799ee', '0', '', '1436773177');
INSERT INTO `oa_file` VALUES ('10', '新媒体.txt', '55a375f5257a2.txt', 'doc/2015-07/', 'txt', 'text/plain', '195', '8ef899adde6afee9e3541fd6e04c1c47', '3fa40ad3dbad064417cf7dd7526f093fc6f799ee', '0', '', '1436775924');
INSERT INTO `oa_file` VALUES ('11', '新媒体.txt', '55a377f264bda.txt', 'doc/2015-07/', 'txt', 'text/plain', '195', '8ef899adde6afee9e3541fd6e04c1c47', '3fa40ad3dbad064417cf7dd7526f093fc6f799ee', '0', '', '1436776434');
INSERT INTO `oa_file` VALUES ('12', '新媒体.txt', '55a37a7219eee.txt', 'doc/2015-07/', 'txt', 'text/plain', '195', '8ef899adde6afee9e3541fd6e04c1c47', '3fa40ad3dbad064417cf7dd7526f093fc6f799ee', '0', '', '1436777073');
INSERT INTO `oa_file` VALUES ('13', '新媒体.txt', '55a37ab1133e8.txt', 'doc/2015-07/', 'txt', 'text/plain', '195', '8ef899adde6afee9e3541fd6e04c1c47', '3fa40ad3dbad064417cf7dd7526f093fc6f799ee', '0', '', '1436777136');
INSERT INTO `oa_file` VALUES ('14', 'customer.xlsx', '55a46cf874704.xlsx', 'doc/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6370', '799d0f7ab2c167b5076eb4abd7b11c96', '165b382fba205725e3c8b9c28f8f01f142da1e57', '0', '', '1436839160');
INSERT INTO `oa_file` VALUES ('15', 'customer.xlsx', '55a46d48ab451.xlsx', 'doc/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6370', '799d0f7ab2c167b5076eb4abd7b11c96', '165b382fba205725e3c8b9c28f8f01f142da1e57', '0', '', '1436839240');
INSERT INTO `oa_file` VALUES ('16', 'customer.xlsx', '55a46fadd5090.xlsx', 'doc/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6370', '799d0f7ab2c167b5076eb4abd7b11c96', '165b382fba205725e3c8b9c28f8f01f142da1e57', '0', '', '1436839853');
INSERT INTO `oa_file` VALUES ('17', 'customer.xlsx', '55a47141b7c35.xlsx', 'doc/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6370', '799d0f7ab2c167b5076eb4abd7b11c96', '165b382fba205725e3c8b9c28f8f01f142da1e57', '0', '', '1436840257');
INSERT INTO `oa_file` VALUES ('18', 'customer.xlsx', '55a48342bb169.xlsx', 'doc/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6370', '799d0f7ab2c167b5076eb4abd7b11c96', '165b382fba205725e3c8b9c28f8f01f142da1e57', '0', '', '1436844866');
INSERT INTO `oa_file` VALUES ('19', 'udf_field.xlsx', '55a4900dbb2a8.xlsx', 'udffield/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6431', '92d0fdf18ca822f554c27e18266fa450', '5ea9eb6cf89a94483450239de62ae6a878fae59e', '0', '', '1436848141');
INSERT INTO `oa_file` VALUES ('20', 'udf_field (1).xlsx', '55a4a49e4b60b.xlsx', 'udffield/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6652', '667ba353b6c169a96de9dd5c04249d66', '798fd80527b7ae61bfaf820b63bfe2b93d4665d0', '0', '', '1436853406');
INSERT INTO `oa_file` VALUES ('21', 'udf_field.xlsx', '55a4a5209b141.xlsx', 'udffield/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6431', '92d0fdf18ca822f554c27e18266fa450', '5ea9eb6cf89a94483450239de62ae6a878fae59e', '0', '', '1436853536');
INSERT INTO `oa_file` VALUES ('22', 'udf_field (1).xlsx', '55a4a5960a3bc.xlsx', 'udffield/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6652', '667ba353b6c169a96de9dd5c04249d66', '798fd80527b7ae61bfaf820b63bfe2b93d4665d0', '0', '', '1436853653');
INSERT INTO `oa_file` VALUES ('23', 'udf_field (1).xlsx', '55a4a6204c5f9.xlsx', 'udffield/2015-07/', 'xlsx', 'application/vnd.openxmlformats-officedoc', '6652', '667ba353b6c169a96de9dd5c04249d66', '798fd80527b7ae61bfaf820b63bfe2b93d4665d0', '0', '', '1436853792');

-- ----------------------------
-- Table structure for `oa_finance`
-- ----------------------------
DROP TABLE IF EXISTS `oa_finance`;
CREATE TABLE `oa_finance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(10) DEFAULT NULL COMMENT '单据编号',
  `remark` varchar(255) DEFAULT NULL,
  `input_date` date DEFAULT NULL COMMENT '录入日期',
  `account_id` int(11) DEFAULT NULL COMMENT '帐号ID',
  `account_name` varchar(20) DEFAULT NULL COMMENT '帐号名',
  `income` int(11) DEFAULT NULL COMMENT '收入',
  `payment` int(11) DEFAULT NULL COMMENT '支出',
  `amount` int(11) DEFAULT NULL COMMENT '合计',
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `partner` varchar(50) DEFAULT NULL COMMENT '来往处',
  `actor_name` varchar(10) DEFAULT NULL COMMENT '经办人',
  `user_id` int(11) DEFAULT NULL COMMENT '登陆人',
  `user_name` varchar(10) DEFAULT NULL COMMENT '登录名',
  `create_time` int(11) DEFAULT NULL COMMENT '创建日期',
  `update_time` int(11) DEFAULT NULL COMMENT '更新日期',
  `add_file` varchar(255) DEFAULT NULL COMMENT '附件',
  `doc_type` tinyint(3) DEFAULT NULL COMMENT '类型',
  `is_del` tinyint(3) DEFAULT '0' COMMENT '删除标记',
  `related_account_id` int(11) DEFAULT NULL COMMENT '相关帐号ID',
  `related_account_name` varchar(20) DEFAULT NULL COMMENT '相关帐号名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_finance
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_finance_account`
-- ----------------------------
DROP TABLE IF EXISTS `oa_finance_account`;
CREATE TABLE `oa_finance_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT '帐号名称',
  `bank` varchar(20) DEFAULT NULL COMMENT '银行',
  `no` varchar(50) DEFAULT NULL COMMENT '银行帐号',
  `init` int(11) DEFAULT NULL COMMENT '初始帐号',
  `balance` int(11) DEFAULT NULL COMMENT '余额',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `is_del` tinyint(3) DEFAULT '0' COMMENT '删除标记',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_finance_account
-- ----------------------------
INSERT INTO `oa_finance_account` VALUES ('5', '农行', '农行', '1688', '1000', null, '农行', '0', '1422690709', null);
INSERT INTO `oa_finance_account` VALUES ('6', '招商', '招商', '1188', '1000', null, '招商', '0', '1422690709', null);
INSERT INTO `oa_finance_account` VALUES ('7', '华夏', '华夏', '8888', '1000', null, '888', '1', '1422690709', null);

-- ----------------------------
-- Table structure for `oa_flow`
-- ----------------------------
DROP TABLE IF EXISTS `oa_flow`;
CREATE TABLE `oa_flow` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(20) NOT NULL DEFAULT '' COMMENT '文档编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `content` text NOT NULL COMMENT '内容',
  `confirm` varchar(200) NOT NULL DEFAULT '' COMMENT '裁决数据',
  `confirm_name` text NOT NULL COMMENT '裁决显示内容',
  `consult` varchar(200) NOT NULL DEFAULT '' COMMENT '协商数据',
  `consult_name` text NOT NULL COMMENT '协商显示内容',
  `refer` varchar(200) NOT NULL DEFAULT '' COMMENT '抄送数据',
  `refer_name` text NOT NULL COMMENT '抄送显示内容',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '流程类型',
  `add_file` varchar(200) NOT NULL DEFAULT '' COMMENT '附件',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `emp_no` varchar(20) DEFAULT NULL COMMENT '员工编号',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名称',
  `dept_id` int(11) NOT NULL DEFAULT '0' COMMENT '部门ID',
  `dept_name` varchar(20) NOT NULL DEFAULT '' COMMENT '部门名称',
  `create_date` varchar(10) NOT NULL DEFAULT '' COMMENT '创建日期',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `step` int(11) NOT NULL DEFAULT '0' COMMENT '目前阶段状态',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `udf_data` text COMMENT '用户自定义数据',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_flow
-- ----------------------------
INSERT INTO `oa_flow` VALUES ('1', '02', '11111', '11111', '1001|', '<span data=\"1001\" id=\"1001\"><nobr><b title=\"李丽/总经理\">李丽/总经理</b></nobr></span>', '', '', '', '', '3', '', '1', 'admin', '管理员', '1', '小微企业', '', '1436853878', '0', '20', '0', '{\"239\":\"11111\",\"240\":\"11111\",\"241\":\"2015-07-07 14:04\",\"242\":\"11111\",\"246\":\"2015-07-15 14:04\",\"247\":\"2015-07-29 14:04\",\"244\":\"11111\",\"245\":\"\",\"243\":\"11111\"}');
INSERT INTO `oa_flow` VALUES ('2', '01', '申请', '离职申请单', 'admin|', '<span data=\"dept_1\" id=\"dept_1\"><nobr><b title=\"小微企业&lt;dept@group&gt;\">小微企业<dept@group></dept@group></b></nobr></span>', '', '', '', '', '1', '', '1', 'admin', '管理员', '1', '小微企业', '', '1436858304', '0', '40', '0', '{\"211\":\"申请\",\"212\":\"2015-07-14\",\"213\":\"申请\",\"214\":\"\",\"225\":\"申请\",\"226\":\"\",\"227\":\"申请\",\"228\":\"\",\"215\":\"申请\",\"216\":\"\",\"217\":\"\"}');
INSERT INTO `oa_flow` VALUES ('3', '01', '申请', '申请', '3002|', '<span data=\"3002\" id=\"3002\"><nobr><b title=\"经理3002/经理\">经理3002/经理</b></nobr></span>', '', '', '', '', '1', '', '1', 'admin', '管理员', '1', '小微企业', '', '1436858375', '0', '20', '0', '{\"211\":\"申请\",\"212\":\"\",\"213\":\"申请\",\"214\":\"\",\"225\":\"申请\",\"226\":\"\",\"227\":\"申请\",\"228\":\"\",\"215\":\"\",\"216\":\"\",\"217\":\"\"}');

-- ----------------------------
-- Table structure for `oa_flow_log`
-- ----------------------------
DROP TABLE IF EXISTS `oa_flow_log`;
CREATE TABLE `oa_flow_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flow_id` int(11) NOT NULL DEFAULT '0' COMMENT '流程ID',
  `emp_no` varchar(20) NOT NULL DEFAULT '' COMMENT '员工编号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_name` varchar(20) DEFAULT '' COMMENT '用户名称',
  `step` int(11) NOT NULL DEFAULT '0' COMMENT '当前步骤',
  `result` int(11) DEFAULT NULL COMMENT '审批结果',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `comment` text COMMENT '意见',
  `is_read` tinyint(3) NOT NULL DEFAULT '0' COMMENT '已读',
  `from` varchar(20) DEFAULT NULL COMMENT '传阅人',
  `is_del` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_flow_log
-- ----------------------------
INSERT INTO `oa_flow_log` VALUES ('1', '1', '1001', '44', '李丽', '21', null, '1436853878', '0', null, '0', null, '0');
INSERT INTO `oa_flow_log` VALUES ('2', '3', '3002', '50', '经理3002', '21', null, '1436858375', '0', null, '0', null, '0');

-- ----------------------------
-- Table structure for `oa_flow_type`
-- ----------------------------
DROP TABLE IF EXISTS `oa_flow_type`;
CREATE TABLE `oa_flow_type` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(20) NOT NULL DEFAULT '' COMMENT '分组',
  `doc_no_format` varchar(50) NOT NULL DEFAULT '' COMMENT '文档编码格式',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `short` varchar(20) NOT NULL DEFAULT '' COMMENT '简称',
  `content` text NOT NULL COMMENT '内容',
  `confirm` varchar(100) NOT NULL DEFAULT '' COMMENT '裁决数据',
  `confirm_name` text NOT NULL COMMENT '裁决显示内容',
  `consult` varchar(100) NOT NULL DEFAULT '' COMMENT '协商数据',
  `consult_name` text NOT NULL COMMENT '协商显示内容',
  `refer` varchar(100) NOT NULL DEFAULT '' COMMENT '抄送数据',
  `refer_name` text NOT NULL COMMENT '抄送显示内容',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_edit` tinyint(3) NOT NULL DEFAULT '0' COMMENT '可编辑标记',
  `is_lock` tinyint(3) NOT NULL DEFAULT '0' COMMENT '锁定标记',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `request_duty` int(11) DEFAULT NULL COMMENT '申请权限',
  `report_duty` int(11) DEFAULT NULL COMMENT '报告权限',
  `udf_tpl` varchar(20) DEFAULT NULL,
  `is_show` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_flow_type
-- ----------------------------
INSERT INTO `oa_flow_type` VALUES ('1', '69', '01', '离职申请单', '离职申请单', '离职申请单', '', '', '', '', '', '', '1436847995', '0', '1', '1', '0', '0', '14', '14', '离职申请单', '1');
INSERT INTO `oa_flow_type` VALUES ('3', '70', '02', '机票预支申请', '机票预支申请', '1', '', '', '', '', '', '', '1436853768', '0', '0', '0', '0', '0', '14', '14', '', '1');

-- ----------------------------
-- Table structure for `oa_form`
-- ----------------------------
DROP TABLE IF EXISTS `oa_form`;
CREATE TABLE `oa_form` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(20) NOT NULL DEFAULT '' COMMENT '文档编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `content` text NOT NULL COMMENT '内容',
  `folder` int(11) NOT NULL DEFAULT '0' COMMENT '文件夹',
  `add_file` varchar(200) NOT NULL DEFAULT '' COMMENT '附件',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名称',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `udf_data` text COMMENT '自定义字段数据',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_form
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_group`
-- ----------------------------
DROP TABLE IF EXISTS `oa_group`;
CREATE TABLE `oa_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `is_public` tinyint(3) DEFAULT NULL COMMENT '是否公开',
  `remark` text COMMENT '备注',
  `user_id` int(11) DEFAULT NULL COMMENT '登陆人ID',
  `user_name` varchar(20) DEFAULT NULL COMMENT '登录用户名称',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `sort` varchar(10) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_group
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_group_user`
-- ----------------------------
DROP TABLE IF EXISTS `oa_group_user`;
CREATE TABLE `oa_group_user` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_group_user
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_info`
-- ----------------------------
DROP TABLE IF EXISTS `oa_info`;
CREATE TABLE `oa_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(20) NOT NULL DEFAULT '' COMMENT '文档编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `content` text NOT NULL COMMENT '内容',
  `folder` int(11) NOT NULL DEFAULT '0' COMMENT '分类',
  `is_sign` tinyint(3) DEFAULT '0' COMMENT '是否需要签收',
  `is_public` tinyint(3) DEFAULT NULL COMMENT '是否公开',
  `scope_user_id` text COMMENT '发布范围ID',
  `scope_user_name` text COMMENT '发布范围名称',
  `add_file` varchar(200) NOT NULL DEFAULT '' COMMENT '附件',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '登陆人ID',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '登陆人名称',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `dept_name` varchar(20) DEFAULT NULL COMMENT '部门名称',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_info
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_info_scope`
-- ----------------------------
DROP TABLE IF EXISTS `oa_info_scope`;
CREATE TABLE `oa_info_scope` (
  `info_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `info_id` (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_info_scope
-- ----------------------------
INSERT INTO `oa_info_scope` VALUES ('109', '1');
INSERT INTO `oa_info_scope` VALUES ('109', '42');
INSERT INTO `oa_info_scope` VALUES ('109', '43');
INSERT INTO `oa_info_scope` VALUES ('109', '44');
INSERT INTO `oa_info_scope` VALUES ('109', '48');
INSERT INTO `oa_info_scope` VALUES ('109', '49');
INSERT INTO `oa_info_scope` VALUES ('109', '50');
INSERT INTO `oa_info_scope` VALUES ('109', '51');
INSERT INTO `oa_info_scope` VALUES ('109', '52');
INSERT INTO `oa_info_scope` VALUES ('109', '55');
INSERT INTO `oa_info_scope` VALUES ('109', '56');
INSERT INTO `oa_info_scope` VALUES ('109', '57');
INSERT INTO `oa_info_scope` VALUES ('109', '58');
INSERT INTO `oa_info_scope` VALUES ('109', '59');
INSERT INTO `oa_info_scope` VALUES ('109', '60');
INSERT INTO `oa_info_scope` VALUES ('109', '61');
INSERT INTO `oa_info_scope` VALUES ('109', '62');
INSERT INTO `oa_info_scope` VALUES ('109', '65');
INSERT INTO `oa_info_scope` VALUES ('109', '66');
INSERT INTO `oa_info_scope` VALUES ('109', '67');
INSERT INTO `oa_info_scope` VALUES ('116', '44');
INSERT INTO `oa_info_scope` VALUES ('116', '67');
INSERT INTO `oa_info_scope` VALUES ('116', '48');
INSERT INTO `oa_info_scope` VALUES ('116', '42');
INSERT INTO `oa_info_scope` VALUES ('116', '43');
INSERT INTO `oa_info_scope` VALUES ('116', '49');
INSERT INTO `oa_info_scope` VALUES ('116', '50');
INSERT INTO `oa_info_scope` VALUES ('116', '51');
INSERT INTO `oa_info_scope` VALUES ('116', '52');
INSERT INTO `oa_info_scope` VALUES ('116', '55');
INSERT INTO `oa_info_scope` VALUES ('116', '57');
INSERT INTO `oa_info_scope` VALUES ('116', '58');
INSERT INTO `oa_info_scope` VALUES ('116', '59');
INSERT INTO `oa_info_scope` VALUES ('116', '60');
INSERT INTO `oa_info_scope` VALUES ('116', '61');
INSERT INTO `oa_info_scope` VALUES ('116', '56');
INSERT INTO `oa_info_scope` VALUES ('116', '62');
INSERT INTO `oa_info_scope` VALUES ('116', '1');
INSERT INTO `oa_info_scope` VALUES ('116', '65');
INSERT INTO `oa_info_scope` VALUES ('116', '66');
INSERT INTO `oa_info_scope` VALUES ('124', '44');
INSERT INTO `oa_info_scope` VALUES ('124', '67');
INSERT INTO `oa_info_scope` VALUES ('124', '48');
INSERT INTO `oa_info_scope` VALUES ('124', '42');
INSERT INTO `oa_info_scope` VALUES ('124', '43');
INSERT INTO `oa_info_scope` VALUES ('124', '49');
INSERT INTO `oa_info_scope` VALUES ('124', '50');
INSERT INTO `oa_info_scope` VALUES ('124', '51');
INSERT INTO `oa_info_scope` VALUES ('124', '52');
INSERT INTO `oa_info_scope` VALUES ('124', '55');
INSERT INTO `oa_info_scope` VALUES ('124', '57');
INSERT INTO `oa_info_scope` VALUES ('124', '58');
INSERT INTO `oa_info_scope` VALUES ('124', '59');
INSERT INTO `oa_info_scope` VALUES ('124', '60');
INSERT INTO `oa_info_scope` VALUES ('124', '61');
INSERT INTO `oa_info_scope` VALUES ('124', '56');
INSERT INTO `oa_info_scope` VALUES ('124', '62');
INSERT INTO `oa_info_scope` VALUES ('124', '1');
INSERT INTO `oa_info_scope` VALUES ('124', '65');
INSERT INTO `oa_info_scope` VALUES ('124', '66');
INSERT INTO `oa_info_scope` VALUES ('125', '44');
INSERT INTO `oa_info_scope` VALUES ('125', '67');
INSERT INTO `oa_info_scope` VALUES ('125', '48');
INSERT INTO `oa_info_scope` VALUES ('125', '42');
INSERT INTO `oa_info_scope` VALUES ('125', '43');
INSERT INTO `oa_info_scope` VALUES ('125', '49');
INSERT INTO `oa_info_scope` VALUES ('125', '50');
INSERT INTO `oa_info_scope` VALUES ('125', '51');
INSERT INTO `oa_info_scope` VALUES ('125', '52');
INSERT INTO `oa_info_scope` VALUES ('125', '55');
INSERT INTO `oa_info_scope` VALUES ('125', '57');
INSERT INTO `oa_info_scope` VALUES ('125', '58');
INSERT INTO `oa_info_scope` VALUES ('125', '59');
INSERT INTO `oa_info_scope` VALUES ('125', '60');
INSERT INTO `oa_info_scope` VALUES ('125', '61');
INSERT INTO `oa_info_scope` VALUES ('125', '56');
INSERT INTO `oa_info_scope` VALUES ('125', '62');
INSERT INTO `oa_info_scope` VALUES ('125', '1');
INSERT INTO `oa_info_scope` VALUES ('125', '65');
INSERT INTO `oa_info_scope` VALUES ('125', '66');
INSERT INTO `oa_info_scope` VALUES ('126', '44');
INSERT INTO `oa_info_scope` VALUES ('127', '44');
INSERT INTO `oa_info_scope` VALUES ('128', '44');
INSERT INTO `oa_info_scope` VALUES ('129', '44');
INSERT INTO `oa_info_scope` VALUES ('130', '48');
INSERT INTO `oa_info_scope` VALUES ('130', '49');
INSERT INTO `oa_info_scope` VALUES ('130', '50');
INSERT INTO `oa_info_scope` VALUES ('130', '55');
INSERT INTO `oa_info_scope` VALUES ('130', '57');
INSERT INTO `oa_info_scope` VALUES ('130', '58');
INSERT INTO `oa_info_scope` VALUES ('130', '59');
INSERT INTO `oa_info_scope` VALUES ('131', '42');
INSERT INTO `oa_info_scope` VALUES ('131', '51');
INSERT INTO `oa_info_scope` VALUES ('131', '52');
INSERT INTO `oa_info_scope` VALUES ('131', '56');
INSERT INTO `oa_info_scope` VALUES ('131', '60');
INSERT INTO `oa_info_scope` VALUES ('131', '61');
INSERT INTO `oa_info_scope` VALUES ('131', '62');
INSERT INTO `oa_info_scope` VALUES ('132', '48');
INSERT INTO `oa_info_scope` VALUES ('132', '49');
INSERT INTO `oa_info_scope` VALUES ('132', '50');
INSERT INTO `oa_info_scope` VALUES ('132', '55');
INSERT INTO `oa_info_scope` VALUES ('132', '57');
INSERT INTO `oa_info_scope` VALUES ('132', '58');
INSERT INTO `oa_info_scope` VALUES ('132', '59');
INSERT INTO `oa_info_scope` VALUES ('133', '48');
INSERT INTO `oa_info_scope` VALUES ('134', '44');
INSERT INTO `oa_info_scope` VALUES ('134', '67');
INSERT INTO `oa_info_scope` VALUES ('134', '48');
INSERT INTO `oa_info_scope` VALUES ('134', '42');
INSERT INTO `oa_info_scope` VALUES ('134', '43');
INSERT INTO `oa_info_scope` VALUES ('134', '49');
INSERT INTO `oa_info_scope` VALUES ('134', '50');
INSERT INTO `oa_info_scope` VALUES ('134', '51');
INSERT INTO `oa_info_scope` VALUES ('134', '52');
INSERT INTO `oa_info_scope` VALUES ('134', '55');
INSERT INTO `oa_info_scope` VALUES ('134', '57');
INSERT INTO `oa_info_scope` VALUES ('134', '58');
INSERT INTO `oa_info_scope` VALUES ('134', '59');
INSERT INTO `oa_info_scope` VALUES ('134', '60');
INSERT INTO `oa_info_scope` VALUES ('134', '61');
INSERT INTO `oa_info_scope` VALUES ('134', '56');
INSERT INTO `oa_info_scope` VALUES ('134', '62');
INSERT INTO `oa_info_scope` VALUES ('134', '1');
INSERT INTO `oa_info_scope` VALUES ('134', '65');
INSERT INTO `oa_info_scope` VALUES ('134', '66');
INSERT INTO `oa_info_scope` VALUES ('135', '48');
INSERT INTO `oa_info_scope` VALUES ('136', '48');
INSERT INTO `oa_info_scope` VALUES ('137', '44');
INSERT INTO `oa_info_scope` VALUES ('137', '67');
INSERT INTO `oa_info_scope` VALUES ('137', '48');
INSERT INTO `oa_info_scope` VALUES ('137', '42');
INSERT INTO `oa_info_scope` VALUES ('137', '43');
INSERT INTO `oa_info_scope` VALUES ('137', '49');
INSERT INTO `oa_info_scope` VALUES ('137', '50');
INSERT INTO `oa_info_scope` VALUES ('137', '51');
INSERT INTO `oa_info_scope` VALUES ('137', '52');
INSERT INTO `oa_info_scope` VALUES ('137', '55');
INSERT INTO `oa_info_scope` VALUES ('137', '57');
INSERT INTO `oa_info_scope` VALUES ('137', '58');
INSERT INTO `oa_info_scope` VALUES ('137', '59');
INSERT INTO `oa_info_scope` VALUES ('137', '60');
INSERT INTO `oa_info_scope` VALUES ('137', '61');
INSERT INTO `oa_info_scope` VALUES ('137', '56');
INSERT INTO `oa_info_scope` VALUES ('137', '62');
INSERT INTO `oa_info_scope` VALUES ('137', '1');
INSERT INTO `oa_info_scope` VALUES ('137', '65');
INSERT INTO `oa_info_scope` VALUES ('137', '66');
INSERT INTO `oa_info_scope` VALUES ('138', '44');
INSERT INTO `oa_info_scope` VALUES ('138', '67');
INSERT INTO `oa_info_scope` VALUES ('138', '48');
INSERT INTO `oa_info_scope` VALUES ('138', '42');
INSERT INTO `oa_info_scope` VALUES ('138', '43');
INSERT INTO `oa_info_scope` VALUES ('138', '49');
INSERT INTO `oa_info_scope` VALUES ('138', '50');
INSERT INTO `oa_info_scope` VALUES ('138', '51');
INSERT INTO `oa_info_scope` VALUES ('138', '52');
INSERT INTO `oa_info_scope` VALUES ('138', '55');
INSERT INTO `oa_info_scope` VALUES ('138', '57');
INSERT INTO `oa_info_scope` VALUES ('138', '58');
INSERT INTO `oa_info_scope` VALUES ('138', '59');
INSERT INTO `oa_info_scope` VALUES ('138', '60');
INSERT INTO `oa_info_scope` VALUES ('138', '61');
INSERT INTO `oa_info_scope` VALUES ('138', '56');
INSERT INTO `oa_info_scope` VALUES ('138', '62');
INSERT INTO `oa_info_scope` VALUES ('138', '1');
INSERT INTO `oa_info_scope` VALUES ('138', '65');
INSERT INTO `oa_info_scope` VALUES ('138', '66');
INSERT INTO `oa_info_scope` VALUES ('2', '42');
INSERT INTO `oa_info_scope` VALUES ('3', '42');
INSERT INTO `oa_info_scope` VALUES ('4', '44');

-- ----------------------------
-- Table structure for `oa_info_sign`
-- ----------------------------
DROP TABLE IF EXISTS `oa_info_sign`;
CREATE TABLE `oa_info_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info_id` int(11) NOT NULL DEFAULT '0' COMMENT '信息ID',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '登录用户ID',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '登录用户名称',
  `is_sign` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否签收',
  `sign_time` int(11) unsigned DEFAULT NULL COMMENT '签收时间',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `dept_name` varchar(20) DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_info_sign
-- ----------------------------
INSERT INTO `oa_info_sign` VALUES ('71', '109', '1', '管理员', '1', '1417533958', null, null);
INSERT INTO `oa_info_sign` VALUES ('72', '124', '1', '管理员', '1', '1422806114', '1', '小微企业');
INSERT INTO `oa_info_sign` VALUES ('73', '124', '42', '总监2001', '1', '1424448156', '8', '财务部');
INSERT INTO `oa_info_sign` VALUES ('74', '130', '55', '员工5001', '1', '1431412695', '24', '总务科');
INSERT INTO `oa_info_sign` VALUES ('75', '130', '48', '副总1003', '1', '1431412995', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('76', '130', '50', '经理3002', '1', '1431413642', '23', '人事科');
INSERT INTO `oa_info_sign` VALUES ('77', '130', '49', '经理3001', '1', '1431413670', '24', '总务科');
INSERT INTO `oa_info_sign` VALUES ('78', '130', '57', '员工5002', '1', '1431413882', '24', '总务科');
INSERT INTO `oa_info_sign` VALUES ('79', '131', '42', '总监2001', '1', '1431414980', '8', '财务部');
INSERT INTO `oa_info_sign` VALUES ('80', '132', '48', '副总1003', '1', '1431415121', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('81', '132', '49', '经理3001', '1', '1431415624', '24', '总务科');
INSERT INTO `oa_info_sign` VALUES ('82', '133', '48', '副总1003', '1', '1431415712', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('83', '133', '48', '副总1003', '1', '1431415716', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('84', '133', '48', '副总1003', '1', '1431415721', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('85', '133', '48', '副总1003', '1', '1431415729', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('86', '133', '48', '副总1003', '1', '1431415732', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('87', '133', '48', '副总1003', '1', '1431415733', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('88', '133', '48', '副总1003', '1', '1431415733', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('89', '134', '48', '副总1003', '1', '1431415924', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('90', '136', '48', '副总1003', '1', '1431416157', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('91', '134', '1', '管理员', '1', '1431416214', '1', '小微企业');
INSERT INTO `oa_info_sign` VALUES ('92', '137', '48', '副总1003', '1', '1431484511', '6', '管理部');
INSERT INTO `oa_info_sign` VALUES ('93', '138', '48', '副总1003', '1', '1431495132', '6', '管理部');

-- ----------------------------
-- Table structure for `oa_mail`
-- ----------------------------
DROP TABLE IF EXISTS `oa_mail`;
CREATE TABLE `oa_mail` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `folder` int(11) NOT NULL DEFAULT '0' COMMENT '文件夹',
  `mid` varchar(200) DEFAULT NULL COMMENT '邮件唯一id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `content` text NOT NULL COMMENT '内容',
  `add_file` varchar(200) DEFAULT NULL COMMENT '附件',
  `from` varchar(255) DEFAULT NULL COMMENT '发件人',
  `to` varchar(255) DEFAULT NULL COMMENT '收件人',
  `reply_to` varchar(255) DEFAULT NULL COMMENT '回复到',
  `cc` varchar(255) DEFAULT NULL COMMENT '抄送',
  `read` tinyint(1) NOT NULL DEFAULT '0' COMMENT '已读',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名称',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_mail
-- ----------------------------
INSERT INTO `oa_mail` VALUES ('1', '4', null, '啊啊啊啊啊', '啊啊啊啊啊啊', 'MDAwMDAwMDAwMIPac3M;', 'OA|576216559@qq.com', '总经办|dept@group|dept@group;', 'OA|576216559@qq.com', '总经办|dept@group|dept@group;', '1', '1', '管理员', '1436757433', '0', '0');
INSERT INTO `oa_mail` VALUES ('2', '4', null, '测试邮件', '测试邮件', '', 'OA测试|1315068148@qq.com', '人事科|dept@group|dept@group;', 'OA测试|1315068148@qq.com', '', '1', '1', '管理员', '1436860706', '0', '0');
INSERT INTO `oa_mail` VALUES ('3', '2', null, '测试', '<h1 style=\"font-size:32px;color:#333333;font-family:微软雅黑;background-color:#FFFFFF;\">\r\n	测试测试测试\r\n</h1>', '', 'OA测试|1315068148@qq.com', '李丽/总经理|576216559@qq.com|576216559@qq.com;', 'OA测试|1315068148@qq.com', '李丽/总经理|576216559@qq.com|576216559@qq.com;', '1', '1', '管理员', '1436860826', '0', '0');

-- ----------------------------
-- Table structure for `oa_mail_account`
-- ----------------------------
DROP TABLE IF EXISTS `oa_mail_account`;
CREATE TABLE `oa_mail_account` (
  `id` mediumint(6) NOT NULL,
  `email` varchar(50) DEFAULT NULL COMMENT '邮件地址',
  `mail_name` varchar(50) NOT NULL DEFAULT '' COMMENT '邮件显示名称',
  `pop3svr` varchar(50) NOT NULL DEFAULT '' COMMENT 'pop服务器',
  `smtpsvr` varchar(50) NOT NULL DEFAULT '' COMMENT 'smtp服务器',
  `mail_id` varchar(50) NOT NULL DEFAULT '' COMMENT '邮件ID',
  `mail_pwd` varchar(50) NOT NULL DEFAULT '' COMMENT '邮件密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='think_user_info';

-- ----------------------------
-- Records of oa_mail_account
-- ----------------------------
INSERT INTO `oa_mail_account` VALUES ('1', '1315068148@qq.com', 'OA测试', 'pop.qq.com', 'smtp.qq.com', '1315068148@qq.com', 'NAxin256128');

-- ----------------------------
-- Table structure for `oa_mail_organize`
-- ----------------------------
DROP TABLE IF EXISTS `oa_mail_organize`;
CREATE TABLE `oa_mail_organize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `sender_check` int(11) NOT NULL DEFAULT '0' COMMENT '是否确认发件人',
  `sender_option` int(11) NOT NULL DEFAULT '0' COMMENT '发件人选项',
  `sender_key` varchar(50) NOT NULL DEFAULT '' COMMENT '确认发件人值',
  `domain_check` int(11) NOT NULL DEFAULT '0' COMMENT '是否确认域名',
  `domain_option` int(11) NOT NULL DEFAULT '0' COMMENT '域名选项',
  `domain_key` varchar(50) NOT NULL DEFAULT '' COMMENT '确认域名值',
  `recever_check` int(11) NOT NULL DEFAULT '0' COMMENT '是否确认收件人',
  `recever_option` int(11) NOT NULL DEFAULT '0' COMMENT '收件人选项',
  `recever_key` varchar(50) NOT NULL DEFAULT '' COMMENT '确认收信人值',
  `title_check` int(11) NOT NULL DEFAULT '0' COMMENT '是否确认标题',
  `title_option` int(11) NOT NULL DEFAULT '0' COMMENT '确认标题选项',
  `title_key` varchar(50) NOT NULL DEFAULT '' COMMENT '确认标题值',
  `to` int(11) NOT NULL DEFAULT '0' COMMENT '移动到',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_mail_organize
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_message`
-- ----------------------------
DROP TABLE IF EXISTS `oa_message`;
CREATE TABLE `oa_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COMMENT '内容',
  `add_file` varchar(200) DEFAULT NULL COMMENT '附件',
  `sender_id` int(11) DEFAULT NULL COMMENT '发送人',
  `sender_name` varchar(20) DEFAULT NULL COMMENT '发送人名称',
  `receiver_id` int(11) DEFAULT NULL COMMENT '接收人',
  `receiver_name` varchar(20) DEFAULT NULL COMMENT '接收人名称',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `owner_id` int(11) DEFAULT NULL COMMENT '拥有者',
  `is_del` tinyint(3) DEFAULT '0' COMMENT '删除标记',
  `is_read` tinyint(3) DEFAULT '0' COMMENT '是否已读',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_message
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_node`
-- ----------------------------
DROP TABLE IF EXISTS `oa_node`;
CREATE TABLE `oa_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT 'URL地址',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `sub_folder` varchar(20) DEFAULT NULL COMMENT '子文件夹',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `sort` varchar(20) DEFAULT NULL COMMENT '排序',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `badge_function` varchar(50) DEFAULT NULL COMMENT '统计函数',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`is_del`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_node
-- ----------------------------
INSERT INTO `oa_node` VALUES ('84', '系统管理', 'System/index', 'fa fa-cogs', '', '', '6', '0', '0', '');
INSERT INTO `oa_node` VALUES ('85', '邮件', 'Mail/index', 'fa fa-envelope-o bc-mail', '', '', '2', '0', '1', 'badge_sum');
INSERT INTO `oa_node` VALUES ('87', '审批', 'Flow/index', 'fa fa-pencil bc-flow', '', '', '4', '0', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('88', '通知管理', 'Info/index', 'fa fa-file-o', '', '', '4', '0', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('91', '日程', 'Schedule/index', 'fa fa-calendar bc-personal-schedule', '', '', '9', '198', '0', 'badge_count_schedule');
INSERT INTO `oa_node` VALUES ('94', '职位', 'Position/index', null, null, '', '', '1', '0', null);
INSERT INTO `oa_node` VALUES ('100', '写信', 'Mail/add', null, '', '', '1', '85', '0', null);
INSERT INTO `oa_node` VALUES ('101', '收件箱', 'Mail/folder?fid=inbox', 'bc-mail-inbox', '', '', '3', '85', '0', 'badge_count_mail_inbox');
INSERT INTO `oa_node` VALUES ('102', '邮件设置', '', null, null, null, '9', '85', '0', null);
INSERT INTO `oa_node` VALUES ('104', '垃圾箱', 'Mail/folder?fid=spambox', '', '', '', '5', '85', '0', null);
INSERT INTO `oa_node` VALUES ('105', '发件箱', 'Mail/folder?fid=outbox', '', '', '', '6', '85', '0', null);
INSERT INTO `oa_node` VALUES ('106', '已删除', 'Mail/folder?fid=delbox', '', '', '', '4', '85', '0', null);
INSERT INTO `oa_node` VALUES ('107', '草稿箱', 'Mail/folder?fid=darftbox', '', '', '', '7', '85', '0', null);
INSERT INTO `oa_node` VALUES ('108', '邮件帐户设置', 'MailAccount/index', null, '', '', '1', '102', '0', null);
INSERT INTO `oa_node` VALUES ('110', '公司信息管理', '', null, null, '', '1', '84', '0', null);
INSERT INTO `oa_node` VALUES ('112', '权限管理', '', null, null, '', '3', '84', '0', null);
INSERT INTO `oa_node` VALUES ('113', '系统设定', '', null, null, '', '4', '84', '0', null);
INSERT INTO `oa_node` VALUES ('114', '系统参数设置', 'SystemConfig/index', '', '', '', '2', '113', '0', '');
INSERT INTO `oa_node` VALUES ('115', '组织图', 'Dept/index', '', '', '', '1', '110', '0', null);
INSERT INTO `oa_node` VALUES ('116', '员工登记', 'User/index', null, '', '', '5', '110', '0', null);
INSERT INTO `oa_node` VALUES ('118', '权限组管理', 'Role/index', '', '', '', '1', '112', '0', null);
INSERT INTO `oa_node` VALUES ('119', '权限设置', 'Role/node', '', '', '', '2', '112', '0', null);
INSERT INTO `oa_node` VALUES ('120', '权限分配', 'Role/user', '', '', '', '3', '112', '0', null);
INSERT INTO `oa_node` VALUES ('121', '菜单管理', 'Node/index', '', '', '', '1', '113', '0', null);
INSERT INTO `oa_node` VALUES ('123', '职位', 'Position/index', null, '', '', '2', '110', '0', null);
INSERT INTO `oa_node` VALUES ('124', '文件夹设置', 'Mail/folder_manage', '', '', '', '2', '102', '0', '');
INSERT INTO `oa_node` VALUES ('125', '联系人', 'Contact/index', '', '', '', '1', '198', '0', null);
INSERT INTO `oa_node` VALUES ('126', '信息搜索', 'Info/index', '', '', '', '1', '88', '0', '');
INSERT INTO `oa_node` VALUES ('143', '邮件分类', 'MailOrganize/index', null, '', '', '', '102', '0', null);
INSERT INTO `oa_node` VALUES ('144', '发起', 'Flow/index', '', '', '', '1', '87', '0', null);
INSERT INTO `oa_node` VALUES ('146', '流程管理', 'FlowType/index', '', '', '', '9', '87', '0', null);
INSERT INTO `oa_node` VALUES ('147', '待审批', 'Flow/folder?fid=confirm', 'bc-flow-confirm', '', '', '4', '87', '0', 'badge_count_flow_todo');
INSERT INTO `oa_node` VALUES ('148', '已审批', 'Flow/folder?fid=finish', '', '', '', '5', '87', '0', '');
INSERT INTO `oa_node` VALUES ('149', '草稿箱', 'Flow/folder?fid=darft', '', '', '', '2', '87', '0', '');
INSERT INTO `oa_node` VALUES ('150', '已提交', 'Flow/folder?fid=submit', '', '', '', '3', '87', '0', '');
INSERT INTO `oa_node` VALUES ('152', '待办', 'Todo/index', 'fa fa-tasks bc-personal-todo', '', '', '9', '198', '0', 'badge_count_todo');
INSERT INTO `oa_node` VALUES ('153', '部门级别', 'DeptGrade/index', '', '', '', '4', '110', '0', null);
INSERT INTO `oa_node` VALUES ('156', '企业管理', 'Customer/index', '', '', '', '2', '157', '0', '');
INSERT INTO `oa_node` VALUES ('157', '企业服务', 'Customer/index', 'fa fa-group', '', '', '2', '0', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('158', '供应商', 'Supplier/index', '', '', '', '3', '157', '1', '');
INSERT INTO `oa_node` VALUES ('169', '职员', 'Staff/index', '', '', '', '', '157', '1', '');
INSERT INTO `oa_node` VALUES ('177', '我的文件夹', '##mail', 'bc-mail-myfolder', 'MailFolder', '', '8', '85', '0', 'badge_count_mail_user_folder');
INSERT INTO `oa_node` VALUES ('184', '流程分组', 'FlowType/tag_manage', '', '', '', '8', '87', '0', null);
INSERT INTO `oa_node` VALUES ('185', '审批报告', 'Flow/folder?fid=report', 'bc-flow-receive', '', '', '6', '87', '0', 'badge_count_flow_receive');
INSERT INTO `oa_node` VALUES ('189', '信息分类', 'Info/folder_manage', '', '', '', 'C1', '88', '0', '');
INSERT INTO `oa_node` VALUES ('190', '消息', 'Message/index', 'fa fa-inbox bc-message', '', '', '99', '198', '0', 'badge_count_message');
INSERT INTO `oa_node` VALUES ('191', '用户设置', '', '', '', '', '', '198', '0', null);
INSERT INTO `oa_node` VALUES ('192', '用户资料', 'Profile/index', '', '', '', '', '191', '0', null);
INSERT INTO `oa_node` VALUES ('193', '修改密码', 'Profile/password', '', '', '', '', '191', '0', null);
INSERT INTO `oa_node` VALUES ('194', '用户设置', 'UserConfig/index', '', '', '', '999', '191', '0', null);
INSERT INTO `oa_node` VALUES ('198', '个人', 'Contact/index', 'fa fa-user bc-personal', '', '', '9', '0', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('205', '业务角色管理', 'Duty/index', '', '', '', '4', '112', '0', '');
INSERT INTO `oa_node` VALUES ('206', '业务权限分配', 'Role/duty', '', '', '', '5', '112', '0', '');
INSERT INTO `oa_node` VALUES ('214', '记账', 'Finance/index', 'fa fa-jpy', '', '', '3', '217', '0', '');
INSERT INTO `oa_node` VALUES ('216', '日报', 'WorkLog/index', 'fa fa-book', '', '', '1', '217', '0', '');
INSERT INTO `oa_node` VALUES ('217', '工作', 'WorkLog/index', 'fa fa-book', '', '', '5', '0', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('219', '我的信息', 'Info/my_info', '', '', '', 'B1', '88', '1', '');
INSERT INTO `oa_node` VALUES ('220', '我的签收', 'Info/my_sign', '', '', '', 'B2', '88', '1', '');
INSERT INTO `oa_node` VALUES ('221', '文档管理', 'Doc/folder?fid=5', 'fa fa-inbox', '', '', '1', '0', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('222', '文档设置', 'Doc/folder_manage', 'fa fa-inbox', '', '', '4', '221', '1', 'badge_sum');
INSERT INTO `oa_node` VALUES ('224', '任务', 'Task/index', 'fa fa-book', '', '', '2', '217', '0', 'badge_count_task');
INSERT INTO `oa_node` VALUES ('226', '报表', 'Form/index##', 'fa fa-table', 'FormFolder', '', '5', '0', '1', 'badge_sum');
INSERT INTO `oa_node` VALUES ('227', '报表管理', 'Form/folder_manage', 'fa fa-inbox', '', '', '4', '226', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('228', '报表字段类型', 'Form/field_type', 'fa fa-inbox', '', '', '4', '226', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('229', '群组', 'Group/index', 'fa fa-group', '', '', '7', '157', '1', 'badge_sum');
INSERT INTO `oa_node` VALUES ('230', '考勤查看', 'Sign/index', 'fa fa-book', '', '', '4', '235', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('231', '考勤统计', 'Sign/report', 'fa fa-book', '', '', '5', '235', '0', 'badge_sum');
INSERT INTO `oa_node` VALUES ('232', '工单', 'WorkOrder/index', 'fa fa-book', '', '', '60', '235', '0', 'badge_count_work_order');
INSERT INTO `oa_node` VALUES ('233', '工单统计', 'WorkOrder/report', 'fa fa-book', '', '', '61', '235', '0', '');
INSERT INTO `oa_node` VALUES ('234', '参阅箱', 'Flow/folder?fid=receive', 'bc-flow-receive', '', '', '6', '87', '0', 'badge_count_flow_receive');
INSERT INTO `oa_node` VALUES ('235', '商业', '', 'fa fa-bar-chart', '', '', '', '0', '1', '');
INSERT INTO `oa_node` VALUES ('236', '招商管理', 'Business/index', 'fa fa-table', '', '', '999', '0', '0', ' badge_sum');
INSERT INTO `oa_node` VALUES ('237', '文件收发', 'Doc/folder?fid=5', '', '', '', '1', '221', '0', '');
INSERT INTO `oa_node` VALUES ('240', '档案 管理', 'Doc/index', '', '', '', '11111', '221', '0', '');
INSERT INTO `oa_node` VALUES ('241', '招商企业', 'Business/index', '', '', '', '1', '236', '0', '');
INSERT INTO `oa_node` VALUES ('242', '楼宇管理', 'Building/index', '', '', '', '99999', '236', '0', '');
INSERT INTO `oa_node` VALUES ('243', '楼宇组织图', 'Building/floor', '', '', '', '1', '242', '0', '');
INSERT INTO `oa_node` VALUES ('244', '办公室管理', 'Room/index', '', '', '', '', '242', '0', '');
INSERT INTO `oa_node` VALUES ('245', '入驻管理', 'Enter/index', '', '', '', '333', '236', '0', '');
INSERT INTO `oa_node` VALUES ('246', '预约审核', 'Enter/status', '', '', null, '', '245', '0', '');
INSERT INTO `oa_node` VALUES ('247', '发布通知', 'Info/folder?fid=6', '', '', null, '', '88', '0', '');

-- ----------------------------
-- Table structure for `oa_position`
-- ----------------------------
DROP TABLE IF EXISTS `oa_position`;
CREATE TABLE `oa_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_no` varchar(10) NOT NULL DEFAULT '' COMMENT '编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `sort` varchar(10) NOT NULL DEFAULT '' COMMENT '排序',
  `is_del` tinyint(3) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_position
-- ----------------------------
INSERT INTO `oa_position` VALUES ('1', '011', '主管', '5', '0');
INSERT INTO `oa_position` VALUES ('2', '04', '经理', '4', '0');
INSERT INTO `oa_position` VALUES ('3', '03', '总监', '3', '0');
INSERT INTO `oa_position` VALUES ('4', '02', '副总', '2', '0');
INSERT INTO `oa_position` VALUES ('5', '01', '总经理', '1', '0');
INSERT INTO `oa_position` VALUES ('6', '06', '助理', '6', '0');

-- ----------------------------
-- Table structure for `oa_push`
-- ----------------------------
DROP TABLE IF EXISTS `oa_push`;
CREATE TABLE `oa_push` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `data` text NOT NULL,
  `status` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `info` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_push
-- ----------------------------
INSERT INTO `oa_push` VALUES ('1', '42', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"测试\",\"content\":\"阿斯顿发送到发撒阿斯顿\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=2\"}', '0', '1436843341', null);
INSERT INTO `oa_push` VALUES ('2', '42', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"法师打发斯蒂芬\",\"content\":\"法师打发斯蒂芬\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=3\"}', '0', '1436843399', null);
INSERT INTO `oa_push` VALUES ('3', '44', '{\"type\":\"审批\",\"action\":\"需要审批\",\"title\":\"11111\",\"content\":\"提交人：小微企业-管理员\",\"url\":\"\\/index.php?m=&c=Flow&a=read&id=1\"}', '0', '1436853878', null);
INSERT INTO `oa_push` VALUES ('4', '44', '{\"type\":\"任务\",\"action\":\"需要执行\",\"title\":\"来自：小微企业-管理员\",\"content\":\"标题：去执行\",\"url\":\"\\/index.php?m=&c=Task&a=read&id=1\"}', '0', '1436854070', null);
INSERT INTO `oa_push` VALUES ('6', '44', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"测试通知\",\"content\":\"测试通知\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=4\"}', '0', '1436857993', null);
INSERT INTO `oa_push` VALUES ('7', '44', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"测试通知\",\"content\":\"测试通知\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=4\"}', '0', '1436858148', null);
INSERT INTO `oa_push` VALUES ('9', '50', '{\"type\":\"审批\",\"action\":\"需要审批\",\"title\":\"申请\",\"content\":\"提交人：小微企业-管理员\",\"url\":\"\\/index.php?m=&c=Flow&a=read&id=3\"}', '0', '1436858375', null);
INSERT INTO `oa_push` VALUES ('10', '3', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"222\",\"content\":\"2222\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=7\"}', '0', '1437105219', null);
INSERT INTO `oa_push` VALUES ('11', '3', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"33333\",\"content\":\"法师打发斯蒂芬爱的发送到\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=8\"}', '0', '1437105488', null);
INSERT INTO `oa_push` VALUES ('12', '3', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"阿斯顿发撒旦\",\"content\":\"阿斯顿\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=9\"}', '0', '1437105757', null);
INSERT INTO `oa_push` VALUES ('13', '3', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"啊大放大\",\"content\":\"阿斯顿\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=10\"}', '0', '1437105824', null);
INSERT INTO `oa_push` VALUES ('14', '4', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"测试\",\"content\":\"测试\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=17\"}', '0', '1437375837', null);
INSERT INTO `oa_push` VALUES ('15', '3', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"lalallaaaaa\",\"content\":\"lalallaaaaalalallaaaaa\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=21\"}', '0', '1437384891', null);
INSERT INTO `oa_push` VALUES ('16', '4', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"会议\",\"content\":\"明天十点举行会议，全体人员参加、\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=24\"}', '0', '1437387048', null);
INSERT INTO `oa_push` VALUES ('17', '4', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"测试开会\",\"content\":\"测试开会\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=25\"}', '0', '1437387165', null);
INSERT INTO `oa_push` VALUES ('18', '3', '{\"type\":\"信息\",\"action\":\"通知 \",\"title\":\"测试开会\",\"content\":\"测试开会\",\"url\":\"\\/index.php?m=&c=Info&a=read&id=25\"}', '0', '0', null);

-- ----------------------------
-- Table structure for `oa_role`
-- ----------------------------
DROP TABLE IF EXISTS `oa_role`;
CREATE TABLE `oa_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `pid` smallint(6) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `sort` varchar(20) DEFAULT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `is_del` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parentId` (`pid`),
  KEY `ename` (`sort`),
  KEY `status` (`is_del`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_role
-- ----------------------------
INSERT INTO `oa_role` VALUES ('1', '公司管理员', '0', '', '1', '1208784792', '1368507168', '0');
INSERT INTO `oa_role` VALUES ('2', '基本权限', '0', '', '2', '1215496283', '1368507164', '0');
INSERT INTO `oa_role` VALUES ('7', '领导', '0', '', '2', '1254325787', '1401288337', '0');

-- ----------------------------
-- Table structure for `oa_role_duty`
-- ----------------------------
DROP TABLE IF EXISTS `oa_role_duty`;
CREATE TABLE `oa_role_duty` (
  `role_id` smallint(6) unsigned NOT NULL,
  `duty_id` smallint(6) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_role_duty
-- ----------------------------
INSERT INTO `oa_role_duty` VALUES ('1', '15');
INSERT INTO `oa_role_duty` VALUES ('7', '14');
INSERT INTO `oa_role_duty` VALUES ('1', '14');
INSERT INTO `oa_role_duty` VALUES ('7', '15');
INSERT INTO `oa_role_duty` VALUES ('2', '14');
INSERT INTO `oa_role_duty` VALUES ('2', '15');

-- ----------------------------
-- Table structure for `oa_role_node`
-- ----------------------------
DROP TABLE IF EXISTS `oa_role_node`;
CREATE TABLE `oa_role_node` (
  `role_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `read` tinyint(1) DEFAULT NULL,
  `write` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_role_node
-- ----------------------------
INSERT INTO `oa_role_node` VALUES ('2', '136', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '135', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '94', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '97', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '98', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '99', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '69', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '6', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '2', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '7', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '131', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '130', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '133', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '132', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '135', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '136', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '117', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '134', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '103', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '133', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '130', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '134', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '132', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '103', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '103', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '109', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '117', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '117', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '117', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '117', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '103', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '109', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '117', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '117', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '163', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '170', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '164', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '155', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '154', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '111', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '168', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '162', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '166', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '161', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '171', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '165', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '174', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '172', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '173', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '160', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '175', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '176', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '167', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '128', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '235', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '230', null, '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '231', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '232', null, '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '233', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '221', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '237', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '240', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '222', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '157', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '169', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '156', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '158', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '229', null, '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '85', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '100', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '101', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '106', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '104', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '105', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '107', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '177', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '102', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '143', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '108', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '124', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '88', null, '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '126', null, '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '219', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '220', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '189', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '87', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '144', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '149', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '150', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '147', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '148', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '234', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '185', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '184', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '146', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '217', null, '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '216', null, '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '224', null, '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '214', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '226', null, '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '227', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '228', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '84', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '110', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '115', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '123', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '153', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '116', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '112', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '118', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '119', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '120', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '205', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '206', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '113', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '121', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '114', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '198', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '191', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '193', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '192', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '194', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '125', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '91', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '152', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '190', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '235', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '230', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '231', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '232', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '233', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '221', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '237', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '240', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '222', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '157', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '169', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '156', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '158', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '229', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '85', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '100', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '101', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '106', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '104', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '105', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '107', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '177', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '102', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '143', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '108', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '124', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '88', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '126', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '219', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '220', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '189', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '87', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '144', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '149', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '150', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '147', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '148', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '234', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '185', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '184', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '146', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '217', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '216', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '224', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '214', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '226', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '227', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '228', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '84', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '110', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '115', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '123', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '153', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '116', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '112', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '118', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '119', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '120', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '205', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '206', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '113', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '121', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '114', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '198', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '191', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '193', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '192', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '194', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '125', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '91', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '152', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '190', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '235', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '230', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '231', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '232', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '233', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '221', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '237', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '240', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '222', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '157', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '169', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '156', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '158', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '229', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '85', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '100', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '101', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '106', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '104', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '105', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '107', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '177', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '102', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '143', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '108', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '124', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '87', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '144', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '149', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '150', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '147', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '148', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '234', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '185', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '184', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '146', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '217', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '216', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '224', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '214', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '226', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '228', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '227', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '84', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '110', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '115', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '123', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '153', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '116', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '112', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '118', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '119', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '120', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '205', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '206', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '113', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '121', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '114', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '198', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '191', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '192', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '193', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '194', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '125', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '91', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '152', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '190', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '236', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '241', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '245', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '246', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '242', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '244', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '243', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '236', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '241', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '245', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '246', null, null, null);
INSERT INTO `oa_role_node` VALUES ('2', '242', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '244', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('2', '243', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '236', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '241', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '245', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '246', null, null, null);
INSERT INTO `oa_role_node` VALUES ('7', '242', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '244', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('7', '243', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '88', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '247', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '126', '1', '1', '1');
INSERT INTO `oa_role_node` VALUES ('1', '219', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '220', null, null, null);
INSERT INTO `oa_role_node` VALUES ('1', '189', null, null, null);

-- ----------------------------
-- Table structure for `oa_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `oa_role_user`;
CREATE TABLE `oa_role_user` (
  `role_id` mediumint(9) unsigned DEFAULT NULL,
  `user_id` char(32) DEFAULT NULL,
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_role_user
-- ----------------------------
INSERT INTO `oa_role_user` VALUES ('4', '27');
INSERT INTO `oa_role_user` VALUES ('4', '26');
INSERT INTO `oa_role_user` VALUES ('5', '31');
INSERT INTO `oa_role_user` VALUES ('3', '22');
INSERT INTO `oa_role_user` VALUES ('1', '4');
INSERT INTO `oa_role_user` VALUES ('1', '3');
INSERT INTO `oa_role_user` VALUES ('1', '35');
INSERT INTO `oa_role_user` VALUES ('1', '36');
INSERT INTO `oa_role_user` VALUES ('1', '54');
INSERT INTO `oa_role_user` VALUES ('2', '3');
INSERT INTO `oa_role_user` VALUES ('1', '53');
INSERT INTO `oa_role_user` VALUES ('7', '36');
INSERT INTO `oa_role_user` VALUES ('1', '2');
INSERT INTO `oa_role_user` VALUES ('2', '2');
INSERT INTO `oa_role_user` VALUES ('7', '2');
INSERT INTO `oa_role_user` VALUES ('1', '63');
INSERT INTO `oa_role_user` VALUES ('1', '64');
INSERT INTO `oa_role_user` VALUES ('2', '41');
INSERT INTO `oa_role_user` VALUES ('2', '68');
INSERT INTO `oa_role_user` VALUES ('1', '67');
INSERT INTO `oa_role_user` VALUES ('2', '67');
INSERT INTO `oa_role_user` VALUES ('7', '67');
INSERT INTO `oa_role_user` VALUES ('1', '65');
INSERT INTO `oa_role_user` VALUES ('2', '65');
INSERT INTO `oa_role_user` VALUES ('7', '65');
INSERT INTO `oa_role_user` VALUES ('1', '66');
INSERT INTO `oa_role_user` VALUES ('2', '66');
INSERT INTO `oa_role_user` VALUES ('7', '66');
INSERT INTO `oa_role_user` VALUES ('1', '44');
INSERT INTO `oa_role_user` VALUES ('2', '44');
INSERT INTO `oa_role_user` VALUES ('7', '44');
INSERT INTO `oa_role_user` VALUES ('1', '48');
INSERT INTO `oa_role_user` VALUES ('2', '48');
INSERT INTO `oa_role_user` VALUES ('7', '48');
INSERT INTO `oa_role_user` VALUES ('1', '42');
INSERT INTO `oa_role_user` VALUES ('2', '42');
INSERT INTO `oa_role_user` VALUES ('7', '42');
INSERT INTO `oa_role_user` VALUES ('1', '43');
INSERT INTO `oa_role_user` VALUES ('2', '43');
INSERT INTO `oa_role_user` VALUES ('7', '43');
INSERT INTO `oa_role_user` VALUES ('1', '49');
INSERT INTO `oa_role_user` VALUES ('2', '49');
INSERT INTO `oa_role_user` VALUES ('7', '49');
INSERT INTO `oa_role_user` VALUES ('1', '50');
INSERT INTO `oa_role_user` VALUES ('2', '50');
INSERT INTO `oa_role_user` VALUES ('7', '50');
INSERT INTO `oa_role_user` VALUES ('1', '51');
INSERT INTO `oa_role_user` VALUES ('2', '51');
INSERT INTO `oa_role_user` VALUES ('7', '51');
INSERT INTO `oa_role_user` VALUES ('1', '52');
INSERT INTO `oa_role_user` VALUES ('2', '52');
INSERT INTO `oa_role_user` VALUES ('7', '52');
INSERT INTO `oa_role_user` VALUES ('1', '55');
INSERT INTO `oa_role_user` VALUES ('2', '55');
INSERT INTO `oa_role_user` VALUES ('7', '55');
INSERT INTO `oa_role_user` VALUES ('1', '57');
INSERT INTO `oa_role_user` VALUES ('2', '57');
INSERT INTO `oa_role_user` VALUES ('7', '57');
INSERT INTO `oa_role_user` VALUES ('1', '58');
INSERT INTO `oa_role_user` VALUES ('2', '58');
INSERT INTO `oa_role_user` VALUES ('7', '58');
INSERT INTO `oa_role_user` VALUES ('1', '59');
INSERT INTO `oa_role_user` VALUES ('2', '59');
INSERT INTO `oa_role_user` VALUES ('7', '59');
INSERT INTO `oa_role_user` VALUES ('1', '60');
INSERT INTO `oa_role_user` VALUES ('2', '60');
INSERT INTO `oa_role_user` VALUES ('7', '60');
INSERT INTO `oa_role_user` VALUES ('1', '61');
INSERT INTO `oa_role_user` VALUES ('2', '61');
INSERT INTO `oa_role_user` VALUES ('7', '61');
INSERT INTO `oa_role_user` VALUES ('1', '56');
INSERT INTO `oa_role_user` VALUES ('2', '56');
INSERT INTO `oa_role_user` VALUES ('7', '56');
INSERT INTO `oa_role_user` VALUES ('1', '62');
INSERT INTO `oa_role_user` VALUES ('2', '62');
INSERT INTO `oa_role_user` VALUES ('7', '62');
INSERT INTO `oa_role_user` VALUES ('1', '1');
INSERT INTO `oa_role_user` VALUES ('2', '1');
INSERT INTO `oa_role_user` VALUES ('7', '1');

-- ----------------------------
-- Table structure for `oa_room`
-- ----------------------------
DROP TABLE IF EXISTS `oa_room`;
CREATE TABLE `oa_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '房间名称',
  `is_del` tinyint(3) NOT NULL,
  `short` varchar(100) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '面积',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '预约状态 0 预约 1 没预约',
  `floor_name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '所属楼层名称',
  `floor_id` int(11) NOT NULL COMMENT '所属楼层id',
  `customer_id` int(11) NOT NULL COMMENT '所属公司id',
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '入驻企业名称',
  `acreage` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sort` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of oa_room
-- ----------------------------
INSERT INTO `oa_room` VALUES ('3', '1301', '0', '1301', '其他', '0', '3层', '6', '3', '2015-06-29', '2015-07-26', '测试的公司名称', '101', '1');
INSERT INTO `oa_room` VALUES ('5', '1101', '0', '1102', '其他', '0', '1层', '3', '0', '2015-01-01', '2016-01-01', '测试的公司名称', '100', '1');
INSERT INTO `oa_room` VALUES ('6', '1201', '0', '1201', '其他', '0', '2层', '5', '3', '2015-06-29', '2015-07-22', '测试的公司名称阿斯顿发送到阿斯顿发', '100平米', '1');
INSERT INTO `oa_room` VALUES ('7', '1102', '0', '1102', '测试办公室测试办公室测试办公室测试办公室测试办公室', '1', '1层', '3', '0', '', '', '', '1000', '2');
INSERT INTO `oa_room` VALUES ('8', '1202', '0', '1202', '', '1', '2层', '5', '0', '', '', '', '100', '2');
INSERT INTO `oa_room` VALUES ('9', '1302', '0', '1302', '', '1', '3层', '6', '0', '', '', '', '100', '2');
INSERT INTO `oa_room` VALUES ('10', '2101', '0', '101', '', '1', '1层', '8', '0', '', '', '', '100', '1');
INSERT INTO `oa_room` VALUES ('11', '2102', '0', '2102', '', '1', '1层', '8', '0', '', '', '', '111', '2');
INSERT INTO `oa_room` VALUES ('12', '2201', '0', '2201', '', '1', '2层', '10', '0', '', '', '', '100', '1');
INSERT INTO `oa_room` VALUES ('13', '2202', '0', '2202', '', '0', '2层', '10', '4', '2015-06-29', '2015-07-23', '测试招商企业', '100', '');

-- ----------------------------
-- Table structure for `oa_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `oa_schedule`;
CREATE TABLE `oa_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '',
  `content` text,
  `location` varchar(50) DEFAULT '',
  `priority` int(11) DEFAULT NULL,
  `actor` varchar(200) DEFAULT '',
  `user_id` int(11) DEFAULT '0',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `add_file` varchar(200) DEFAULT '',
  `is_del` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_schedule
-- ----------------------------
INSERT INTO `oa_schedule` VALUES ('1', '去执行', '去执行去执行', '', '3', '', '1', '2015-07-14 14:09:28', '2015-07-22 14:08:00', '', '0');

-- ----------------------------
-- Table structure for `oa_sign`
-- ----------------------------
DROP TABLE IF EXISTS `oa_sign`;
CREATE TABLE `oa_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `emp_no` varchar(10) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `latitude` varchar(10) DEFAULT NULL,
  `longitude` varchar(10) DEFAULT NULL,
  `precision` varchar(10) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `is_real_time` tinyint(3) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `sign_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_sign
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `oa_supplier`;
CREATE TABLE `oa_supplier` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `letter` varchar(50) DEFAULT '',
  `short` varchar(30) DEFAULT '',
  `account` varchar(20) DEFAULT '',
  `tax_no` varchar(20) DEFAULT '',
  `payment` varchar(20) DEFAULT NULL,
  `contact` varchar(20) NOT NULL DEFAULT '',
  `office_tel` varchar(20) DEFAULT NULL,
  `mobile_tel` varchar(20) DEFAULT '',
  `email` varchar(50) DEFAULT '',
  `im` varchar(20) DEFAULT '',
  `address` varchar(50) DEFAULT '',
  `user_id` int(11) NOT NULL,
  `is_del` tinyint(3) NOT NULL DEFAULT '0',
  `remark` text,
  `fax` varchar(255) DEFAULT NULL,
  `user_name` varchar(21) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_supplier
-- ----------------------------
INSERT INTO `oa_supplier` VALUES ('1', '阿斯顿发撒旦', 'ASDFSD', '是打发', '阿斯顿', ' ', '爱的', '阿斯顿发', '15104613361', '大水法', '阿斯顿', '阿斯顿', '大放送', '1', '0', '', '阿斯顿', null);

-- ----------------------------
-- Table structure for `oa_system_config`
-- ----------------------------
DROP TABLE IF EXISTS `oa_system_config`;
CREATE TABLE `oa_system_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `val` varchar(255) DEFAULT NULL,
  `is_del` tinyint(3) NOT NULL DEFAULT '0',
  `sort` varchar(20) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_system_config
-- ----------------------------
INSERT INTO `oa_system_config` VALUES ('1', 'SYSTEM_NAME', '系统名称', '小微OA', '0', '', '0');
INSERT INTO `oa_system_config` VALUES ('7', 'UPLOAD_FILE_TYPE', '上传文件类型', 'doc,docx,xls,xlsx,ppt,pptx,pdf,gif,png,tif,zip,rar,jpg,jpeg,txt', '0', null, '0');
INSERT INTO `oa_system_config` VALUES ('8', 'IS_VERIFY_CODE', '验证码', '0', '0', '', '0');
INSERT INTO `oa_system_config` VALUES ('15', 'FINANCE_INCOME_TYPE', '办公费', '办公费', '0', '1', '28');
INSERT INTO `oa_system_config` VALUES ('16', 'FINANCE_INCOME_TYPE', '通讯费', '通讯费', '0', '2', '28');
INSERT INTO `oa_system_config` VALUES ('17', 'FINANCE_PAYMENT_TYPE', '办公费', '办公费', '0', '', '29');
INSERT INTO `oa_system_config` VALUES ('18', 'FINANCE_PAYMENT_TYPE', '通讯费', '通讯费', '0', '', '29');
INSERT INTO `oa_system_config` VALUES ('19', 'WEIXIN_CORP_ID', 'WEIXIN_CORP_ID', 'wx4124a601419ba115', '0', '', '27');
INSERT INTO `oa_system_config` VALUES ('20', 'WEIXIN_SECRET', 'WEIXIN_SECRET', 'TlnidZYom5z8T-pE0y_O7IXm7dSPgPDtI3nQ7RyqLQePiyUXzWo8F-qum1n4i_QM', '0', '', '27');
INSERT INTO `oa_system_config` VALUES ('21', 'WEIXIN_TOKEN', 'WEIXIN_TOKEN', 'xiaowei', '0', '', '27');
INSERT INTO `oa_system_config` VALUES ('22', 'WEIXIN_ENCODING_AES_KEY', 'WEIXIN_ENCODING_AES_KEY', 'lsBzWprOjjdbkMatbg54wwMC2H9ZXmi1aEdDmUQ2nPq', '0', '', '27');
INSERT INTO `oa_system_config` VALUES ('23', 'WEIXIN_SITE_URL', 'WEIXIN_SITE_URL', 'http://xiaowei.smeoa.com', '0', '', '27');
INSERT INTO `oa_system_config` VALUES ('24', 'FINANCE_PAYMENT_TYPE', '餐费', '餐费', '0', '', '29');
INSERT INTO `oa_system_config` VALUES ('25', 'OA_AGENT_ID', 'OA_AGENT_ID', '5', '0', '', '27');
INSERT INTO `oa_system_config` VALUES ('26', 'WEIXIN_SUBSCRIBE_MSG', 'WEIXIN_SUBSCRIBE_MSG', '欢迎使用微信办公系统。', '0', '', '27');
INSERT INTO `oa_system_config` VALUES ('27', '微信设置', '微信设置', '微信设置', '0', '', '0');
INSERT INTO `oa_system_config` VALUES ('28', '记账-收入', '记账-收入', '记账-收入', '0', '', '0');
INSERT INTO `oa_system_config` VALUES ('29', '记账-支出', '记账-支出', '记账-支出', '0', '', '0');
INSERT INTO `oa_system_config` VALUES ('31', 'WS_PUSH_CONFIG', 'WS_PUSH_CONFIG', '消息', '0', '', '0');

-- ----------------------------
-- Table structure for `oa_system_folder`
-- ----------------------------
DROP TABLE IF EXISTS `oa_system_folder`;
CREATE TABLE `oa_system_folder` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `controller` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `admin` varchar(200) NOT NULL,
  `write` varchar(200) NOT NULL,
  `read` varchar(200) NOT NULL,
  `sort` varchar(20) NOT NULL,
  `is_del` tinyint(3) NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_system_folder
-- ----------------------------
INSERT INTO `oa_system_folder` VALUES ('1', '0', 'Doc', '我是测试文档', '经理3001/经理|3001;', '员工5001/主管|5001;', '员工5001/主管|5001;', '1', '1', '');
INSERT INTO `oa_system_folder` VALUES ('2', '0', 'Doc', '档案管理', '小微企业|dept_1;', '小微企业|dept_1;', '小微企业|dept_1;', '3', '0', '');
INSERT INTO `oa_system_folder` VALUES ('3', '0', 'Doc', '通知管理 ', '小微企业|dept_1;', '小微企业|dept_1;', '小微企业|dept_1;', '3', '1', '');
INSERT INTO `oa_system_folder` VALUES ('4', '0', 'Doc', '通知管理', '小微企业|dept_1;', '小微企业|dept_1;', '小微企业|dept_1;', '10', '1', '');
INSERT INTO `oa_system_folder` VALUES ('5', '0', 'Doc', '文件收发', '小微企业|dept_1;', '小微企业|dept_1;', '小微企业|dept_1;', '1', '0', '1111111');
INSERT INTO `oa_system_folder` VALUES ('6', '0', 'Info', '通知 ', '小微企业|dept_1;', '小微企业|dept_1;', '小微企业|dept_1;', '1', '0', '');
INSERT INTO `oa_system_folder` VALUES ('7', '0', 'Doc', '测试一下', '小微企业|dept_1;', '小微企业|dept_1;', '小微企业|dept_1;', '6999', '1', '');
INSERT INTO `oa_system_folder` VALUES ('8', '0', 'Doc', '111111111111111111', '小微企业|dept_1;总务科|dept_24;经理3001/经理|3001;员工5001/主管|5001;员工5002/主管|5002;', '小微企业|dept_1;', '小微企业|dept_1;', '9898989898989898', '1', '');
INSERT INTO `oa_system_folder` VALUES ('9', '0', 'Doc', '档案管理', '小微企业|dept_1;', '小微企业|dept_1;', '小微企业|dept_1;', '22222', '1', '');
INSERT INTO `oa_system_folder` VALUES ('10', '0', 'Info', '发布通知', '总经办|dept_5;', '总经办|dept_5;', '总经办|dept_5;', '', '0', '');

-- ----------------------------
-- Table structure for `oa_system_log`
-- ----------------------------
DROP TABLE IF EXISTS `oa_system_log`;
CREATE TABLE `oa_system_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `data` float DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_system_log
-- ----------------------------
INSERT INTO `oa_system_log` VALUES ('17', '1', '1433756245', '66', null);
INSERT INTO `oa_system_log` VALUES ('18', '2', '1433756245', '45', null);
INSERT INTO `oa_system_log` VALUES ('19', '1', '1433756245', '66', null);
INSERT INTO `oa_system_log` VALUES ('20', '2', '1433756245', '45', null);
INSERT INTO `oa_system_log` VALUES ('21', '1', '1433756246', '66', null);
INSERT INTO `oa_system_log` VALUES ('22', '2', '1433756246', '45', null);
INSERT INTO `oa_system_log` VALUES ('23', '1', '1433756246', '66', null);
INSERT INTO `oa_system_log` VALUES ('24', '2', '1433756246', '45', null);
INSERT INTO `oa_system_log` VALUES ('25', '1', '1433756246', '66', null);
INSERT INTO `oa_system_log` VALUES ('26', '2', '1433756246', '45', null);
INSERT INTO `oa_system_log` VALUES ('27', '1', '1433756247', '66', null);
INSERT INTO `oa_system_log` VALUES ('28', '2', '1433756247', '45', null);
INSERT INTO `oa_system_log` VALUES ('29', '1', '1433756255', '66', null);
INSERT INTO `oa_system_log` VALUES ('30', '2', '1433756255', '45', null);
INSERT INTO `oa_system_log` VALUES ('31', '1', '1433756255', '66', null);
INSERT INTO `oa_system_log` VALUES ('32', '2', '1433756255', '45', null);
INSERT INTO `oa_system_log` VALUES ('33', '1', '1433756255', '66', null);
INSERT INTO `oa_system_log` VALUES ('34', '2', '1433756255', '45', null);
INSERT INTO `oa_system_log` VALUES ('35', '1', '1433756256', '66', null);
INSERT INTO `oa_system_log` VALUES ('36', '2', '1433756256', '45', null);
INSERT INTO `oa_system_log` VALUES ('37', '1', '1433756271', '66', null);
INSERT INTO `oa_system_log` VALUES ('38', '2', '1433756271', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('39', '1', '1433756272', '66', null);
INSERT INTO `oa_system_log` VALUES ('40', '2', '1433756272', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('41', '1', '1433756272', '66', null);
INSERT INTO `oa_system_log` VALUES ('42', '2', '1433756272', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('43', '1', '1433756272', '66', null);
INSERT INTO `oa_system_log` VALUES ('44', '2', '1433756272', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('45', '1', '1433756277', '66', null);
INSERT INTO `oa_system_log` VALUES ('46', '2', '1433756277', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('47', '1', '1433756277', '66', null);
INSERT INTO `oa_system_log` VALUES ('48', '2', '1433756277', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('49', '1', '1433756278', '66', null);
INSERT INTO `oa_system_log` VALUES ('50', '2', '1433756278', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('51', '1', '1433756278', '66', null);
INSERT INTO `oa_system_log` VALUES ('52', '2', '1433756278', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('53', '1', '1433756292', '66', null);
INSERT INTO `oa_system_log` VALUES ('54', '2', '1433756292', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('55', '1', '1433756293', '66', null);
INSERT INTO `oa_system_log` VALUES ('56', '2', '1433756293', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('57', '1', '1433756293', '66', null);
INSERT INTO `oa_system_log` VALUES ('58', '2', '1433756293', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('59', '1', '1433756293', '66', null);
INSERT INTO `oa_system_log` VALUES ('60', '2', '1433756293', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('61', '1', '1433756295', '66', null);
INSERT INTO `oa_system_log` VALUES ('62', '2', '1433756295', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('63', '1', '1433756295', '66', null);
INSERT INTO `oa_system_log` VALUES ('64', '2', '1433756295', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('65', '1', '1433756296', '66', null);
INSERT INTO `oa_system_log` VALUES ('66', '2', '1433756296', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('67', '1', '1433756296', '66', null);
INSERT INTO `oa_system_log` VALUES ('68', '2', '1433756296', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('69', '1', '1433756297', '66', null);
INSERT INTO `oa_system_log` VALUES ('70', '2', '1433756297', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('71', '1', '1433756298', '66', null);
INSERT INTO `oa_system_log` VALUES ('72', '2', '1433756298', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('73', '1', '1433756298', '66', null);
INSERT INTO `oa_system_log` VALUES ('74', '2', '1433756298', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('75', '1', '1433756298', '66', null);
INSERT INTO `oa_system_log` VALUES ('76', '2', '1433756298', '45.0893', null);
INSERT INTO `oa_system_log` VALUES ('77', '1', '1436497592', '0', null);
INSERT INTO `oa_system_log` VALUES ('78', '2', '1436497592', '0', null);
INSERT INTO `oa_system_log` VALUES ('79', '1', '1436750602', '0', null);
INSERT INTO `oa_system_log` VALUES ('80', '2', '1436750602', '0', null);
INSERT INTO `oa_system_log` VALUES ('81', '1', '1436837381', '13', null);
INSERT INTO `oa_system_log` VALUES ('82', '2', '1436837381', '0.00216007', null);
INSERT INTO `oa_system_log` VALUES ('83', '1', '1436923842', '23', null);
INSERT INTO `oa_system_log` VALUES ('84', '2', '1436923842', '0.0638323', null);
INSERT INTO `oa_system_log` VALUES ('85', '1', '1437010248', '23', null);
INSERT INTO `oa_system_log` VALUES ('86', '2', '1437010248', '0.0638323', null);
INSERT INTO `oa_system_log` VALUES ('87', '1', '1437096880', '23', null);
INSERT INTO `oa_system_log` VALUES ('88', '2', '1437096880', '0.0638323', null);
INSERT INTO `oa_system_log` VALUES ('89', '1', '1437356253', '23', null);
INSERT INTO `oa_system_log` VALUES ('90', '2', '1437356253', '0.0638323', null);
INSERT INTO `oa_system_log` VALUES ('91', '1', '1437443329', '23', null);
INSERT INTO `oa_system_log` VALUES ('92', '2', '1437443329', '0.0638323', null);

-- ----------------------------
-- Table structure for `oa_system_tag`
-- ----------------------------
DROP TABLE IF EXISTS `oa_system_tag`;
CREATE TABLE `oa_system_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `controller` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `sort` varchar(20) NOT NULL,
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_system_tag
-- ----------------------------
INSERT INTO `oa_system_tag` VALUES ('58', '0', 'Supplier', '123', '', '');
INSERT INTO `oa_system_tag` VALUES ('59', '0', 'Doc', '123123', '123123', '');
INSERT INTO `oa_system_tag` VALUES ('60', '0', 'Doc', '123123', '', '');
INSERT INTO `oa_system_tag` VALUES ('61', '0', 'Doc', 'abc', '', '');
INSERT INTO `oa_system_tag` VALUES ('65', '0', 'Customer', '22222', '', '');
INSERT INTO `oa_system_tag` VALUES ('66', '0', 'Flow', '人事', '', '');
INSERT INTO `oa_system_tag` VALUES ('67', '0', 'Flow', 'IT', '', '');
INSERT INTO `oa_system_tag` VALUES ('68', '0', 'Flow', '公告', '', '');
INSERT INTO `oa_system_tag` VALUES ('69', '0', 'FlowType', '人事', '1', '');
INSERT INTO `oa_system_tag` VALUES ('70', '0', 'FlowType', '行政', '2', '');
INSERT INTO `oa_system_tag` VALUES ('71', '0', 'FlowType', '采购', '3', '');
INSERT INTO `oa_system_tag` VALUES ('72', '0', 'FlowType', '出差', '4', '');
INSERT INTO `oa_system_tag` VALUES ('80', '0', 'FlowType', '车辆管理', '5', '');
INSERT INTO `oa_system_tag` VALUES ('81', '0', 'Customer', '测试新组', '', '');

-- ----------------------------
-- Table structure for `oa_system_tag_data`
-- ----------------------------
DROP TABLE IF EXISTS `oa_system_tag_data`;
CREATE TABLE `oa_system_tag_data` (
  `row_id` int(11) NOT NULL DEFAULT '0',
  `tag_id` int(11) NOT NULL DEFAULT '0',
  `controller` varchar(20) NOT NULL DEFAULT '',
  KEY `row_id` (`row_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_system_tag_data
-- ----------------------------
INSERT INTO `oa_system_tag_data` VALUES ('18', '58', 'Supplier');
INSERT INTO `oa_system_tag_data` VALUES ('29', '14', 'Video');
INSERT INTO `oa_system_tag_data` VALUES ('29', '46', 'Video');
INSERT INTO `oa_system_tag_data` VALUES ('30', '46', 'Video');
INSERT INTO `oa_system_tag_data` VALUES ('30', '47', 'Video');
INSERT INTO `oa_system_tag_data` VALUES ('30', '56', 'Video');
INSERT INTO `oa_system_tag_data` VALUES ('64', '59', 'Doc');
INSERT INTO `oa_system_tag_data` VALUES ('64', '60', 'Doc');
INSERT INTO `oa_system_tag_data` VALUES ('64', '61', 'Doc');
INSERT INTO `oa_system_tag_data` VALUES ('65', '61', 'Doc');
INSERT INTO `oa_system_tag_data` VALUES ('33', '14', 'Video');
INSERT INTO `oa_system_tag_data` VALUES ('17', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('18', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('19', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('20', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('21', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('22', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('23', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('17', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('18', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('19', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('20', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('21', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('22', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('23', '66', 'Flow');
INSERT INTO `oa_system_tag_data` VALUES ('17', '70', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('23', '72', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('22', '72', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('21', '72', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('20', '72', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('24', '70', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('32', '65', 'Customer');
INSERT INTO `oa_system_tag_data` VALUES ('18', '71', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('19', '71', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('33', '71', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('34', '71', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('35', '71', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('36', '71', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('42', '70', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('46', '70', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('47', '70', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('48', '70', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('49', '70', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('50', '70', 'FlowType');
INSERT INTO `oa_system_tag_data` VALUES ('2', '70', 'FlowType');

-- ----------------------------
-- Table structure for `oa_task`
-- ----------------------------
DROP TABLE IF EXISTS `oa_task`;
CREATE TABLE `oa_task` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `task_no` varchar(20) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text COMMENT '内容',
  `executor` varchar(200) DEFAULT NULL,
  `add_file` varchar(255) DEFAULT NULL,
  `expected_time` datetime DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT '0',
  `user_name` varchar(20) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `dept_name` varchar(20) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `update_user_name` varchar(20) DEFAULT NULL,
  `status` tinyint(3) DEFAULT '0',
  `is_del` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_task
-- ----------------------------
INSERT INTO `oa_task` VALUES ('1', '2015-0001', null, '去执行', '去执行去执行去执行去执行去执行去执行去执行', '李丽|44;', '', '2015-07-30 14:07:00', '1', '管理员', '1', '小微企业', '1436854070', null, null, null, '0', '0');
INSERT INTO `oa_task` VALUES ('2', '2015-0002', null, '去执行', '去执行去执行', '小微企业|dept_1;', '', '2015-07-22 14:08:00', '1', '管理员', '1', '小微企业', '1436854105', null, null, null, '2', '0');

-- ----------------------------
-- Table structure for `oa_task_log`
-- ----------------------------
DROP TABLE IF EXISTS `oa_task_log`;
CREATE TABLE `oa_task_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `type` tinyint(3) DEFAULT NULL,
  `assigner` int(11) DEFAULT NULL COMMENT '分配任务的人',
  `executor` varchar(20) DEFAULT NULL COMMENT '执行人',
  `executor_name` varchar(20) DEFAULT NULL,
  `status` tinyint(3) DEFAULT '0',
  `plan_time` datetime DEFAULT NULL,
  `transactor_name` varchar(20) DEFAULT NULL,
  `transactor` int(11) DEFAULT NULL COMMENT '由谁处理的',
  `finish_rate` tinyint(3) DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `feed_back` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_task_log
-- ----------------------------
INSERT INTO `oa_task_log` VALUES ('1', '1', '1', '1', '44', '李丽', '0', null, null, null, null, null, null);
INSERT INTO `oa_task_log` VALUES ('2', '2', '2', '1', '1', '小微企业', '2', '2015-06-30 14:09:00', '管理员', '1', '27', null, '');

-- ----------------------------
-- Table structure for `oa_todo`
-- ----------------------------
DROP TABLE IF EXISTS `oa_todo`;
CREATE TABLE `oa_todo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `end_date` varchar(10) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `add_file` varchar(200) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_todo
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_udf_field`
-- ----------------------------
DROP TABLE IF EXISTS `oa_udf_field`;
CREATE TABLE `oa_udf_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `row_type` int(11) NOT NULL,
  `sort` varchar(20) NOT NULL,
  `msg` varchar(50) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `layout` varchar(255) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `validate` varchar(20) DEFAULT NULL,
  `controller` varchar(20) DEFAULT NULL,
  `is_del` tinyint(3) DEFAULT '0',
  `config` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_udf_field
-- ----------------------------
INSERT INTO `oa_udf_field` VALUES ('59', '姓名', '33', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('60', '申请日期', '33', '', '', 'date', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('61', '所属部门', '33', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('62', '所属科室', '33', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('64', 'B1', '76', '', '', 'text', '1', '', '', 'Flow', '0', null);
INSERT INTO `oa_udf_field` VALUES ('65', 'B2', '76', '', '', 'text', '1', '', '', 'Flow', '0', null);
INSERT INTO `oa_udf_field` VALUES ('66', '职别', '76', '', '', 'text', '1', '', '', 'Form', '0', 'show|col-10');
INSERT INTO `oa_udf_field` VALUES ('67', '姓名', '76', '', '', 'date', '1', '', '', 'Form', '0', 'show|col-10');
INSERT INTO `oa_udf_field` VALUES ('68', '最高学历', '76', '', '', 'radio', '2', '1111|2222,,33333|.444444', '', 'Form', '0', '');
INSERT INTO `oa_udf_field` VALUES ('69', '姓名', '34', '', '请填写姓名', 'datetime', '1', '', 'require', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('71', '申请日期', '34', '', '请填写联系电话', 'text', '1', '', 'require', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('72', '请假开始时间', '34', '', '请选择开始时间', 'datetime', '1', '', 'require', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('73', '请假结束时间', '34', '', '请选择束时间', 'datetime', '1', '', 'require', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('74', '请假原因', '34', '', '请填写请假原因', 'textarea', '2', '', 'require', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('76', '姓名', '38', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('77', '申请日期', '38', '', '', 'date', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('78', '所属部门', '38', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('79', '所属科室', '38', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('80', '姓名', '36', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('82', '申请日期', '36', '', '', 'date', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('83', '所属部门、科室', '36', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('85', '姓名', '44', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('86', '部门/科室', '44', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('87', '离职申请日期', '44', '', '', 'date', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('88', '离职理由', '44', '', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('89', '培训申请人', '54', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('90', '部门/科室', '54', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('91', '培训地点', '54', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('92', '培训开始时间', '54', '4', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('93', '培训目的', '54', '6', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('94', '培训内容', '54', '7', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('95', '姓名', '41', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('96', '部门/科室', '41', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('97', '出差地点', '41', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('98', '出差开始时间', '41', '5', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('99', '预计费用', '41', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('100', '出差事由/目的', '41', '7', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('103', '姓名', '40', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('104', '部门/科室', '40', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('105', '请假开始时间', '40', '', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('106', '请假结束时间', '40', '', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('107', '请假事由', '40', '', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('108', '培训结束时间', '54', '5', '', 'datetime', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('109', '出差结束时间', '41', '6', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('110', '申请人', '43', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('112', '部门/科室', '43', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('114', '用途', '43', '3', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('115', '申请人', '55', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('116', '部门/科室', '55', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('117', '预算依据', '55', '4', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('118', '预算用途', '55', '3', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('119', '预算明细请参考附件', '55', '5', '', 'text', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('120', '申请人', '45', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('121', '部门/科室', '45', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('122', '出差地点', '45', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('123', '出差开始时间', '45', '5', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('124', '出差结束时间', '45', '6', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('125', '住宿费用', '45', '7', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('126', '交通费用', '45', '8', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('127', '餐费', '45', '9', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('128', '补贴', '45', '91', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('129', '申请人', '19', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('130', '部门/科室', '19', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('131', '招待地点', '19', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('132', '招待时间', '19', '5', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('133', '招待费用', '19', '7', '', 'text', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('134', '招待目的', '19', '8', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('135', '招待对象', '19', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('136', '参加人员', '19', '6', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('137', '出差目的', '45', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('138', '申请人', '53', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('139', '部门/科室', '53', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('140', '辅设备名', '53', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('141', '数量', '53', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('142', '型号规格', '53', '5', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('143', '预计费用', '53', '6', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('144', '申请理由', '53', '7', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('145', '申请人', '51', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('146', '部门/科室', '51', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('147', '设备名称', '51', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('148', '数量', '51', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('149', '型号规格', '51', '5', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('150', '预计费用', '51', '6', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('151', '申请理由', '51', '7', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('152', '申请人', '52', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('153', '部门/科室', '52', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('154', '辅材名称', '52', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('155', '数量', '52', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('156', '型号规格', '52', '5', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('157', '预计费用', '52', '6', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('158', '申请理由', '52', '7', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('159', '申请人', '39', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('160', '部门/科室', '39', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('161', '原材料名称', '39', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('162', '数量', '39', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('163', '型号规格', '39', '5', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('164', '预计费用', '39', '6', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('165', '申请理由', '39', '7', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('166', '申请人', '50', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('167', '部门/科室', '50', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('168', '申请时间', '50', '3', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('169', '出差地点', '50', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('170', '出差事由', '50', '9', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('171', '去程', '50', '6', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('172', '回程', '50', '7', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('173', '出差开始时间', '50', '51', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('176', '出差结束时间', '50', '52', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('177', '申请人', '49', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('178', '部门/科室', '49', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('179', '目的/用途', '49', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('180', '使用开始时间', '49', '4', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('181', '使用结束时间', '49', '5', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('182', '申请时间', '49', '21', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('183', '参会人', '49', '6', '', 'text', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('184', '申请人', '42', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('185', '部门/科室', '42', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('186', '申请理由', '42', '3', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('187', '办公用品名称以及数量', '42', '4', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('189', '申请时间', '42', '21', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('190', '申请人', '46', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('191', '部门/科室', '46', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('192', '申请时间', '46', '3', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('193', '申请事由', '46', '5', '', 'text', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('194', '通讯报销金额', '46', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('195', '申请人', '47', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('196', '部门/科室', '47', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('197', '申请日期', '47', '3', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('199', '出差开始时间', '47', '4', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('200', '出差结束时间', '47', '5', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('201', '出差事由', '47', '31', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('202', '实际使用费用', '47', '6', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('203', '标准费用', '47', '7', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('204', '交通费用明细', '47', '8', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('205', '申请人', '48', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('206', '部门/科室', '48', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('207', '申请时间', '48', '3', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('208', '实际费用', '48', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('209', '参加人员', '48', '5', '', 'text', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('210', '申请事由', '48', '41', '', 'text', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('211', '姓名', '1', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('212', '申请日期', '1', '', '', 'date', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('213', '所属部门', '1', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('214', '所属科室', '1', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('215', '申请人', '1', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('216', '部门/科室', '1', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('217', '申请时间', '1', '3', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('225', '姓名', '1', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('226', '申请日期', '1', '', '', 'date', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('227', '所属部门', '1', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('228', '所属科室', '1', '', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('230', '申请人', '2', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('231', '部门/科室', '2', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('232', '申请时间', '2', '3', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('233', '出差地点', '2', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('235', '去程', '2', '6', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('236', '回程', '2', '7', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('237', '出差开始时间', '2', '51', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('238', '出差结束时间', '2', '52', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('239', '申请人', '3', '1', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('240', '部门/科室', '3', '2', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('241', '申请时间', '3', '3', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('242', '出差地点', '3', '4', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('243', '出差事由', '3', '9', '', 'editor', '2', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('244', '去程', '3', '6', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('245', '回程', '3', '7', '', 'text', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('246', '出差开始时间', '3', '51', '', 'datetime', '1', '', '', 'Flow', '0', '');
INSERT INTO `oa_udf_field` VALUES ('247', '出差结束时间', '3', '52', '', 'datetime', '1', '', '', 'Flow', '0', '');

-- ----------------------------
-- Table structure for `oa_user`
-- ----------------------------
DROP TABLE IF EXISTS `oa_user`;
CREATE TABLE `oa_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emp_no` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(20) NOT NULL,
  `letter` varchar(10) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `rank_id` int(11) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `birthday` date DEFAULT NULL,
  `last_login_ip` varchar(40) DEFAULT NULL,
  `login_count` int(8) DEFAULT NULL,
  `pic` varchar(200) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `duty` varchar(2000) NOT NULL,
  `office_tel` varchar(20) NOT NULL,
  `mobile_tel` varchar(20) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `is_del` tinyint(3) NOT NULL DEFAULT '0',
  `openid` varchar(50) DEFAULT NULL,
  `westatus` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`emp_no`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_user
-- ----------------------------
INSERT INTO `oa_user` VALUES ('1', 'admin', '管理员', 'GLY', '21232f297a57a5a743894a0e4a801fc3', '1', '1', '2', 'male', '2013-09-18', '127.0.0.1', '3035', 'Uploads/emp_pic/1.jpeg', '1315068148@qq.com', '1231254123123', '5086-2222-2222', '12123123', '1222907803', '1436860373', '0', '', '1');
INSERT INTO `oa_user` VALUES ('41', '2002', '总监2002', 'ZJ', '4ba29b9f9e5732ed33761840f4ba6c53', '6', '3', '1', 'male', '2013-10-30', '0.0.0.0', null, '', '', '行政，财务', '', '', '1376896154', '1407565312', '1', null, '1');
INSERT INTO `oa_user` VALUES ('42', '2001', '总监2001', 'ZJ', 'd0fb963ff976f9c37fc81fe03c21ea7b', '8', '3', '1', 'male', '2013-10-10', '127.0.0.1', null, 'emp_pic/42.jpeg', '', '开发', '123', '12312312', '1380970837', '1401287019', '0', '12312541231251243123', '1');
INSERT INTO `oa_user` VALUES ('43', '2003', '总监2003', 'ZJ', 'a591024321c5e2bdbd23ed35f0574dde', '7', '3', '1', 'male', '0000-00-00', '127.0.0.1', null, 'Uploads/emp_pic/43.jpeg', '', '销售', '', '', '1381035116', '1432107034', '0', null, '1');
INSERT INTO `oa_user` VALUES ('44', '1001', '李丽', 'LL', 'b8c37e33defde51cf91e1e03e51657da', '5', '5', '5', 'male', '0000-00-00', '127.0.0.1', null, 'http://img.smeoa.com/popup/2015-04/552a1c4ba8627.jpg', '576216559@qq.com', '全面管理', '', '18560630025', '1381502796', '1436860765', '0', '', '1');
INSERT INTO `oa_user` VALUES ('48', '1003', '副总1003', 'FZ', 'aa68c75c4a77c87f97fb686b2f068676', '6', '4', '1', 'female', '0000-00-00', '192.168.1.106', null, '', '', '销售，运营', '', '', '1381503490', '1401286413', '0', null, '1');
INSERT INTO `oa_user` VALUES ('49', '3001', '经理3001', 'JL', '908c9a564a86426585b29f5335b619bc', '24', '2', '2', 'male', '2013-10-10', '127.0.0.1', null, '', '', '财务', '123', '12312312', '1391694170', '1401287097', '0', null, '1');
INSERT INTO `oa_user` VALUES ('50', '3002', '经理3002', 'JL', 'd806ca13ca3449af72a1ea5aedbed26a', '23', '2', '2', 'male', '2013-10-10', '192.168.1.110', null, '', '', '人事', '123', '12312312', '1391694193', '1401287121', '0', null, null);
INSERT INTO `oa_user` VALUES ('51', '3003', '经理3003', 'JL', 'a4380923dd651c195b1631af7c829187', '25', '2', '2', 'male', '2013-10-10', '192.168.1.106', null, '', '', '销售-南方区域', '123', '12312312', '1391694198', '1401287147', '0', null, null);
INSERT INTO `oa_user` VALUES ('52', '3004', '经理3004', 'JL', '20479c788fb27378c2c99eadcf207e7f', '26', '2', '2', 'male', '2013-10-10', null, null, '', '', '新产品研发', '123', '12312312', '1391694202', '1401287170', '0', null, null);
INSERT INTO `oa_user` VALUES ('55', '5001', '员工5001', 'YG', '03b264c595403666634ac75d828439bc', '24', '1', '3', 'male', '2013-10-10', '192.168.1.106', null, '', '', '会计', '123', '12312312', '1391694320', '1401287296', '0', null, null);
INSERT INTO `oa_user` VALUES ('56', '5007', '员工5007', 'YG', '351869bde8b9d6ad1e3090bd173f600d', '26', '1', '3', 'male', '2013-10-10', '127.0.0.1', null, '', '', '程序员3', '123', '12312312', '1391694335', '1401287430', '0', null, null);
INSERT INTO `oa_user` VALUES ('57', '5002', '员工5002', 'YG', '415585bd389b69659223807d77a96791', '24', '1', '3', 'male', '2013-10-10', '192.168.1.106', null, '', '', '出纳', '123', '12312312', '1391694413', '1401287322', '0', null, null);
INSERT INTO `oa_user` VALUES ('58', '5003', '员工5003', 'YG', '240ac9371ec2671ae99847c3ae2e6384', '23', '1', '3', 'male', '2013-10-10', '127.0.0.1', null, '', '', '招聘，薪资管理', '123', '12312312', '1391694420', '1401287339', '0', null, null);
INSERT INTO `oa_user` VALUES ('59', '5004', '员工5004', 'YG', '3202111cf90e7c816a472aaceb72b0df', '23', '1', '3', 'male', '2013-10-10', '127.0.0.1', null, '', '', '负责公司日常采购', '123', '12312312', '1391694427', '1401287371', '0', null, null);
INSERT INTO `oa_user` VALUES ('60', '5005', '员工5005', 'YG', '1d6408264d31d453d556c60fe7d0459e', '25', '1', '3', 'male', '2013-10-10', '127.0.0.1', null, '', '', '程序员1', '123', '12312312', '1391694435', '1401287390', '0', null, null);
INSERT INTO `oa_user` VALUES ('61', '5006', '员工5006', 'YG', '2c27a260f16ad3098393cc529f391f4a', '25', '1', '3', 'male', '2013-10-10', '127.0.0.1', null, '', '', '程序员2', '123', '12312312', '1391694478', '1401287410', '0', null, null);
INSERT INTO `oa_user` VALUES ('62', '5008', '员工5008', 'YG', 'b3848d61bbbc6207c6668a8a9e2730ed', '26', '1', '3', 'male', '2013-10-10', '127.0.0.1', null, '', '', '网站编辑，SEO', '123', '12312312', '1391694489', '1401287450', '0', null, null);

-- ----------------------------
-- Table structure for `oa_user_config`
-- ----------------------------
DROP TABLE IF EXISTS `oa_user_config`;
CREATE TABLE `oa_user_config` (
  `id` int(11) NOT NULL DEFAULT '0',
  `home_sort` varchar(255) DEFAULT NULL,
  `list_rows` int(11) DEFAULT '20',
  `readed_info` text,
  `push_web` varchar(255) DEFAULT NULL,
  `push_wechat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_user_config
-- ----------------------------
INSERT INTO `oa_user_config` VALUES ('1', 'undefined11,12,|21,22,', '10', '2,3,4', 'mail,flow,info,task,message', 'mail,flow,info,task,message');
INSERT INTO `oa_user_config` VALUES ('42', 'undefined,11,12,|undefined,21,13,', '20', '131', null, null);
INSERT INTO `oa_user_config` VALUES ('43', 'undefined,11,12,|undefined,21,13,', '20', null, null, null);
INSERT INTO `oa_user_config` VALUES ('44', 'undefined12,11,|21,22,', '20', null, null, null);
INSERT INTO `oa_user_config` VALUES ('48', null, '20', '135,136,137,138', null, null);
INSERT INTO `oa_user_config` VALUES ('49', 'undefined,11,12,|13,undefined,21,', '20', '130,132', null, null);
INSERT INTO `oa_user_config` VALUES ('50', null, '20', '130', null, null);
INSERT INTO `oa_user_config` VALUES ('51', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('52', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('53', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('54', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('55', null, '20', '130', null, null);
INSERT INTO `oa_user_config` VALUES ('56', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('57', null, '20', '130', null, null);
INSERT INTO `oa_user_config` VALUES ('58', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('59', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('60', 'undefined,22,13,23,|undefined,12,21,11,', '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('61', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('62', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('63', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('64', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('65', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('66', '11,12,13,|21,22,23,', '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('67', null, '20', '54,55,56', null, null);
INSERT INTO `oa_user_config` VALUES ('68', 'undefined,11,13,|undefined,21,12,', '20', '54,55,56', null, null);

-- ----------------------------
-- Table structure for `oa_user_folder`
-- ----------------------------
DROP TABLE IF EXISTS `oa_user_folder`;
CREATE TABLE `oa_user_folder` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `controller` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `sort` varchar(20) NOT NULL,
  `is_del` tinyint(3) NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_user_folder
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_user_tag`
-- ----------------------------
DROP TABLE IF EXISTS `oa_user_tag`;
CREATE TABLE `oa_user_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `controller` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `sort` varchar(20) NOT NULL,
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_user_tag
-- ----------------------------
INSERT INTO `oa_user_tag` VALUES ('21', '0', 'Contact', '1', '123', '', '');

-- ----------------------------
-- Table structure for `oa_user_tag_data`
-- ----------------------------
DROP TABLE IF EXISTS `oa_user_tag_data`;
CREATE TABLE `oa_user_tag_data` (
  `row_id` int(11) NOT NULL DEFAULT '0',
  `tag_id` int(11) NOT NULL DEFAULT '0',
  `controller` varchar(20) NOT NULL DEFAULT '',
  KEY `row_id` (`row_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_user_tag_data
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_work_log`
-- ----------------------------
DROP TABLE IF EXISTS `oa_work_log`;
CREATE TABLE `oa_work_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(20) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `dept_name` varchar(20) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `content` text,
  `plan` text,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `is_del` tinyint(3) NOT NULL DEFAULT '0',
  `add_file` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_work_log
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_work_order`
-- ----------------------------
DROP TABLE IF EXISTS `oa_work_order`;
CREATE TABLE `oa_work_order` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `task_no` varchar(20) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text COMMENT '内容',
  `executor` varchar(200) DEFAULT NULL,
  `actor` varchar(200) DEFAULT '',
  `add_file` varchar(255) DEFAULT NULL,
  `request_arrive_time` datetime DEFAULT NULL,
  `request_finish_time` datetime DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT '0',
  `user_name` varchar(20) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `dept_name` varchar(20) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `update_user_name` varchar(20) DEFAULT NULL,
  `status` tinyint(3) DEFAULT '0',
  `is_del` tinyint(3) DEFAULT '0',
  `other` varchar(20) DEFAULT NULL,
  `arrive_time` int(11) DEFAULT NULL,
  `finish_time` int(11) DEFAULT NULL,
  `arrive_lat` varchar(10) DEFAULT NULL,
  `arrive_lng` varchar(10) DEFAULT NULL,
  `finish_lat` varchar(10) DEFAULT NULL,
  `finish_lng` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_work_order
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_work_order_log`
-- ----------------------------
DROP TABLE IF EXISTS `oa_work_order_log`;
CREATE TABLE `oa_work_order_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `type` tinyint(3) DEFAULT NULL,
  `assigner` int(11) DEFAULT NULL COMMENT '分配任务的人',
  `executor` varchar(20) DEFAULT NULL COMMENT '执行人',
  `executor_name` varchar(20) DEFAULT NULL,
  `status` tinyint(3) DEFAULT '0',
  `arrive_time` int(11) DEFAULT NULL,
  `transactor_name` varchar(20) DEFAULT NULL,
  `transactor` int(11) DEFAULT NULL COMMENT '由谁处理的',
  `finish_rate` tinyint(3) DEFAULT NULL,
  `finish_time` int(11) DEFAULT NULL,
  `feed_back` text,
  `arrive_lat` varchar(10) DEFAULT NULL,
  `arrive_lng` varchar(10) DEFAULT NULL,
  `finish_lat` varchar(10) DEFAULT NULL,
  `finish_lng` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oa_work_order_log
-- ----------------------------
