<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class DeptController extends HomeController {

	protected $config = array('app_type' => 'master');
	public function index() {
		$node = M("Dept");
		$menu = array();
		//系统管里公司组织图显示当前登录部门管理
       
		$menu = $node -> where($where)->field('id,pid,name,is_del') -> order('sort asc') -> select();
		$tree = list_to_tree($menu);
		//判断是否为管理员
		$this -> assign('menu', popup_tree_menuzz($tree));
		$model = M("Dept");
		$list = $model -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_list', $list);

		$model = M("DeptGrade");
        $where['is_del']= 0;
		$list = $model -> where($where) -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_grade_list', $list);

		$this -> display();
	}

	public function add() {
		$model = M("DeptGrade");	
		$where['is_del']=0; 
		$list = $model -> where($where) -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_grade_list', $list);
		$this -> display();
	}

	public function edit($id){
		$vo = M('Dept') -> where("id=$id") -> find();
		$this -> assign('vo', $vo);
		$model = M("DeptGrade");		
        $where['is_del']= 0;
		$list = $model -> where($where) -> order('sort asc') -> getField('id,name');
		$jd = M('Task_jd') -> where("tid=$id") -> select('id,name');
		$this -> assign('dept_grade_list', $list);
		$this -> display();
	}

	public function savee(){      
		 M('Dept')->where(array('id'=>$_POST['id'])) -> save($_POST);
		 $this -> success('编辑成功');


	}

	public function del($id) {
		$this -> _destory($id);
	}

	public function winpop() {
		$node = M("Dept");
		$menu = array();	
		$where['is_del']=0; 
		$menu = $node -> where($where) -> field('id,pid,name') -> order('sort asc') -> select();	
		$tree = list_to_tree($menu);
		
		$this -> assign('menu', popup_tree_menu($tree ));
		$pid = array();
		$this -> assign('pid', $pid);
		$this -> display();
	}

	public function winpop2() {
		$this -> winpop();
	}

}
?>