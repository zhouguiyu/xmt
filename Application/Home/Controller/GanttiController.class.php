<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
namespace Home\Controller;

class GanttiController extends HomeController {
  protected $config = array('app_type' => 'common', 'read' => 'ajaxjd,readgtt,let_me_do,accept,reject,save_log');


  public function index() {
      $this -> display();
      require('./gtt/index.php'); 
  }

   public function readgtt($id) {
      require('./gtt/index.php');     
      $jd = M("Task_jd") ->where(array('tid'=>$id))->select();
      $this -> assign('id', $id);
      $this -> assign('jd', $jd);
      $this -> display(); 
  }
   public function ajaxjd($key,$id) {  	    
	    $jd = M("Task_jd") ->where(array('tid'=>$id))->select();	   
	    echo json_encode($jd[$key]);
   }
}
