<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
--------------------------------------------------------------*/

namespace Home\Controller;

class DocController extends HomeController {
	protected $config = array('app_type' => 'folder', 'admin' => 'del,move_to,folder_manage,folder,saveedit');
	//过滤查询字段
	function _search_filter(&$map) {
		$map['is_del'] = array('eq', '0');
		$keyword = I('keyword');
		if (!empty($keyword) && empty($map['64'])) {
			$map['name'] = array('like', "%" . $keyword . "%");
		}
	}

	public function index() {
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);

		$map = $this -> _search();		
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		$folder_list = D("SystemFolder") -> get_authed_folder();
		if (!empty($folder_list)) {
			$map['folder'] = array("in", $folder_list);
		} else {
			$map['_string'] = '1=2';
		}

		$model = D("DocView");
	
		//屏蔽是否归档字段
		//print_r($map);exit;
		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> display();
	}

	public function edit($id) {
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);
		$user_id = get_user_id();	
		//判断是否为管理员
		if($user_id!='1'){
            $user_name = M('User') -> where("id=$user_id") -> getField('name');
            $this -> assign("user_name", $user_name);
            $this -> assign("user_id", $user_id);
        }

		$model = M("Doc");
		$folder_id = $model -> where("id=$id") -> getField('folder');
        $this -> assign("folder", $folder_id);
		$this -> assign("auth", D("SystemFolder") -> get_folder_auth($folder_id));
		$this -> _edit($id);
	}
    public function  saveedit(){
        $doc = array(
            'add_file'=>$_POST['add_file'],
            'name'=>$_POST['name'],
            'doc_number'=>$_POST['doc_number'],
            'name_type'=>$_POST['name_type'],
            'keyword'=>$_POST['keyword'],
            'file_type'=>$_POST['file_type'],
            'gd_name'=>$_POST['gd_name'],
            'gd_id'=>$_POST['gd_id'],
        );
        M('doc')->where(array('id'=>$_POST[id]))->save($doc);
        if($_POST['notation']!=''){
            $doc_notation = array(
                'doc_id'=>$_POST['id'],
                'user_id'=>$_POST['user_id'],
                'user_name'=>$_POST['user_name'],
                'notation'=>$_POST['notation'],
                'create_time'=>time(),
            );
            M('doc_notation')->add($doc_notation);
        }
        $this->success('编辑成功', '/index.php?m=&c=Doc&a=folder&fid='.$_POST['folder']);
    }

	public function folder($fid) {
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);
		$this -> assign('auth', $this -> config['auth']);

		$model = D("Doc");
		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}
        if($_GET['projectid']){
        	$map['projectid'] =$_GET['projectid'];
        	$this -> assign("projectid", $_GET['projectid']);
        }
		$map['folder'] = $fid;

		// $user_id = get_user_id();	
		// if($user_id!='1'){
		// 	$parent_id = M('User') -> where("id=$user_id") -> getField('parent_deptid');
		// 	$map['parent_id'] = array('eq', $parent_id);
  //           $where = array ('is_del'=>0,'parent_deptid'=>$parent_id);
  //           $folder_list = M("SystemFolder") -> where($where) -> getField("name,id,name,pid,sort");
  //           $tree = list_to_tree($folder_list);
  //           $this -> assign("folder_list", dropdown_menu($tree));

  //       }else{
            $this -> _assign_folder_list();
        // }
			
		if (!empty($model)) {
			$arr =$this -> _list($model, $map);
		}

		$where = array();
		$where['id'] = array('eq', $fid);		
		$folder_name = M("SystemFolder") -> where($where) -> getField("name");
		$this -> assign("folder_name", $folder_name);
		$this -> assign("folder", $fid);
		$this -> display();
		return;
	}

	public function add($fid) {

		if($_GET['projectid']){
			$this -> assign("projectid", $_GET['projectid']);	
		}
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);
  
		$this -> assign('folder', $fid);
		$this -> display();
	}

	public function read($id) {
		$model = M("Doc");
		$folder_id = $model -> where("id=$id") -> getField('folder');
		$this -> assign("auth", D("SystemFolder") -> get_folder_auth($folder_id));
        $notation = M('doc_notation') -> where("doc_id=$id") -> select();
        $this -> assign("notation",$notation);
		$this -> _edit($id);
	}

	public function del($id) {
		$where['id'] = array('in', $id);
		$folder = M("Doc") -> distinct(true) -> where($where) -> getField('folder',true);
		if (count($folder) == 1) {
			$auth = D("SystemFolder") -> get_folder_auth($folder[0]);
			if ($auth['admin'] == true) {
				$this -> _del($id);
			}
		} else {
			$return['info'] = "删除失败";
			$return['status'] = 0;
			$this -> ajaxReturn($return);
		}
	}

	public function move_to($id, $val) {
		$target_folder = $val;
		$where['id'] = array('in', $id);
		$folder = M("Doc") -> distinct(true) -> where($where) ->  getField('folder',true);
		if (count($folder) == 1) {
			$auth = D("SystemFolder") -> get_folder_auth($folder[0]);
			if ($auth['admin'] == true) {
				$field = 'folder';
				$result = $this -> _set_field($id, $field, $target_folder);

				if ($result) {
					$return['info'] = "操作成功";
					$return['status'] = 1;
					$this -> ajaxReturn($return);
				} else {
					$return['info'] = "操作失败";
					$return['status'] = 1;
					$this -> ajaxReturn($return);
				}
			}
		} else {
			$return['info'] = "操作成功";
			$return['status'] = 1;
			$this -> ajaxReturn($return);
		}
	}
	
	function folder_manage(){
		$this->_system_folder_manage('文档管理',true);
	} 
	
	function upload() {
		$this -> _upload();
	}

	function down($attach_id) {
		$this -> _down($attach_id);
	}


}
