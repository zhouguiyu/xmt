<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class CustomerDeptController extends HomeController {

	protected $config = array('app_type' => 'master');

	public function index($id) {  	  	
		$node = M("Customer_dept");
		$menu = array();
		$where =array('customer_id'=>$id);
		//系统管里公司组织图显示当前登录部门管理
		$menu = $node ->where($where)->field('id,pid,name,is_del') -> order('sort asc') -> select();
		$tree = list_to_tree($menu);		
	    $this -> assign('id',$id);	
		$this -> assign('menu', popup_tree_menu($tree));
		$model = M("customer_grade");
        $map= array('customer_id'=>$id,'is_del'=>0);
		$list = $model -> where($map) -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_grade_list', $list);    
        $cdept =M("Cdept")->where(array('is_del'=>0))->select();
        $this -> assign('cdept', $cdept);
        $this -> display();
	}

	public function add($id) {
		$user_id = get_user_id();	
        $cdept =M("Cdept")->where(array('is_del'=>0))->select();
        $this -> assign('cdept', $cdept);
		$model = M("Customer_grade");
		$this -> assign('id',$id);	
		$where =array('customer_id'=>$id,'is_del'=>0);
		$list = $model -> where($where) -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_grade_list', $list);

		$this -> display();
	}

	public function del($id) {
		$this -> _destory($id);
	}

	public function winpop($id) {		
		$node = M("Customer_dept");
		$menu = array();
		$where =array('is_del'=>0,'customer_id'=>$id);
		$menu = $node -> where($where) -> field('id,pid,name') -> order('sort asc') -> select();
		$tree = list_to_tree($menu);	
		$this -> assign('menu', popup_tree_menu($tree));
		$pid = array();
		$this -> assign('pid', $pid);
		$this -> display();
	}

	public function winpop2() {

		$this -> display();
	}

}
?>