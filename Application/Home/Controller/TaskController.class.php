<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
namespace Home\Controller;

class TaskController extends HomeController {
	protected $config = array('app_type' => 'common', 'read' => 'jd_delete,saveedit,saveadd,let_me_do,accept,reject,save_log');

	//过滤查询字段
	function _search_filter(&$map) {
		$map['is_del'] = array('eq', '0');
		if (!empty($_REQUEST['keyword']) && empty($map['64'])) {
			$map['name'] = array('like', "%" . $_POST['keyword'] . "%");
		}
	}

	public function index() {
		$this -> redirect('folder', array('fid' => 'all'));
	}

	public function folder() {
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);
		$this -> assign('auth', $this -> config['auth']);
		$this -> assign('user_id', get_user_id());

		$where = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($where);
		}

		$todo_task_count = badge_count_todo_task();
		$dept_task_count = badge_count_dept_task();
		$no_assign_task_count = badge_count_no_assign_task();
				
		$this -> assign('todo_task_count', $todo_task_count);
		$this -> assign('dept_task_count', $dept_task_count);
		$this -> assign('no_assign_task_count', $no_assign_task_count);

		$fid = $_GET['fid'];
		$this -> assign("fid", $fid);
		switch ($fid) {
			case 'all' :
				$this -> assign("folder_name", '所有任务');			
				break;
			case 'todo' :
				$this -> assign("folder_name", '等待我接受的任务');
				$where_log['type'] = 1;
				$where_log['status'] = array('eq', '0');
				$where_log['executor'] = get_user_id();
				$task_list = M("TaskLog") -> where($where_log) -> getField('task_id', TRUE);
				if (empty($task_list)) {
					$where['_string'] = '1=2';
				} else {
					$where['id'] = array('in', $task_list);
				}

				break;

			case 'dept' :
				$this -> assign("folder_name", '我们部门的任务');
				$auth = $this -> config['auth'];

				if ($auth['admin']) {
					$where_log['type'] = 2;
					$where_log['status'] = array('eq', '0');
					$where_log['executor'] = get_dept_id();
					$task_list = M("TaskLog") -> where($where_log) -> getField('task_id', TRUE);
					if (empty($task_list)) {
						$where['_string'] = '1=2';
					} else {
						$where['id'] = array('in', $task_list);
					}
				} else {
					$where['_string'] = '1=2';
				}
				break;

			case 'no_assign' :
				$this -> assign("folder_name", '不知让谁处理的任务');

				$prefix = C('DB_PREFIX');

				$assign_list = M("Task") -> getField('id', true);

				$sql = "select id from {$prefix}task task where status=0 and not exists (select * from {$prefix}task_log task_log where task.id=task_log.task_id)";
				$task_list = M() -> query($sql);
			
				if (empty($task_list)) {
					$where['_string'] = '1=2';
				} else {
					foreach ($task_list as $key => $val) {
						$list[] = $val['id'];
					}
					$where['id'] = array('in', $list);
				}

				break;

			case 'no_finish' :
				$this -> assign("folder_name", '我未完成的任务');

				$where_log['status'] = array('lt', 2);
				$where_log['executor'] = get_user_id();
				$where_log['type'] = array('eq', 1);

				$task_list = M("TaskLog") -> where($where_log) -> getField('task_id', true);
				if (empty($task_list)) {
					$where['_string'] = '1=2';
				} else {
					$where['id'] = array('in', $task_list);
				}

				break;

			case 'finished' :
				$this -> assign("folder_name", '我已完成的任务');

				$where_log['executor'] = get_user_id();
				$where_log['type'] = array('eq', 1);

				$task_list = M("TaskLog") -> where($where_log) -> getField('task_id', true);
				if (empty($task_list)) {
					$where['_string'] = '1=2';
				} else {
					$where['id'] = array('in', $task_list);
					$where['status'] = array('eq', 3);
				}
				break;

			case 'my_task' :
				$this -> assign("folder_name", '我发布的任务');
				$where['user_id'] = get_user_id();
				break;
			case 'my_assign' :
				$this -> assign("folder_name", '我指派的任务');

				$where_log['assigner'] = get_user_id();
				$task_list = M("TaskLog") -> where($where_log) -> getField('task_id', TRUE);
				if (empty($task_list)) {
					$where['_string'] = '1=2';
				} else {
					$where['id'] = array('in', $task_list);
				}
				break;

			default :
				break;
		}
		$model = D('Task');
		if (!empty($model)) {
			$this -> _list($model, $where);
		}
		$this -> display();
	}

	public function edit($id) {
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$plugin['date'] = true;

		$jd = M('Task_jd') -> where("tid=$id") -> select();
		$this -> assign("plugin", $plugin);
        $this -> assign("jd", $jd);
		$model = M("Task");
		$this -> _edit($id);
	}

	public function del($id) {
		$this -> _del($id);
	}

	public function add() {
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);
		$user_id = get_user_id();
		$name = M('User') -> where("id=$user_id") -> getField('name');
		$this -> assign('name', $name);	
		$this -> display();
	}

	public function read($id) {
		$plugin['jquery-ui'] = true;
		$plugin['editor'] = true;
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);
		$this -> assign('auth', $this -> config['auth']);

		$this -> assign('task_id', $id);
		$model = M("Task");
		$vo = $model -> find($id);
		$this -> assign('vo', $vo);

		$where_log['task_id'] = array('eq', $id);
		$task_log = M("TaskLog") -> where($where_log) -> select();
		$this -> assign('task_log', $task_log);
		if (empty($vo['executor'])) {
			$this -> assign('no_assign', 1);
		} else {

		}

		$where_accept['status'] = 0;
		$where_accept['task_id'] = $id;
		$where_accept['type'] = 1;
		$where_accept['executor'] = array('eq', get_user_id());
		$task_accept = M("TaskLog") -> where($where_accept) -> find();

		if ($task_accept) {
			$this -> assign('is_accept', 1);
			$this -> assign('task_log_id', $task_accept['id']);
		}

		if ($this -> config['auth']['admin']) {
			$where_dept_accept['status'] = 0;
			$where_dept_accept['task_id'] = $id;
			$where_dept_accept['type'] = 2;
			$where_dept_accept['executor'] = array('eq', get_dept_id());
			$task_dept_accept = M("TaskLog") -> where($where_dept_accept) -> find();
			if ($task_dept_accept) {
				$this -> assign('is_accept', 1);
				$this -> assign('task_log_id', $task_dept_accept['id']);
			}
		}

		$where_working['status'] = array('in', '1,2');
		$where_working['task_id'] = $id;
		$where_working['transactor'] = array('eq', get_user_id());
		$task_working = M("TaskLog") -> where($where_working) -> find();
	    $jd = M('Task_jd') -> where("tid=$id") -> select();
	    $this -> assign('jd', $jd);
		if ($task_working) {
			$this -> assign('is_working', 1);
			$this -> assign('task_working', $task_working);

		}
		$this -> display();
	}

	function let_me_do($task_id) {
		if (IS_POST) {
			M("Task") -> where("id=$task_id") -> setField('executor', get_user_name() . "|" . get_user_id());
			M("Task") -> where("id=$task_id") -> setField('status', 1);

			$data['task_id'] = I(task_id);
			$data['executor'] = get_user_id();
			$data['executor_name'] = get_user_name();
			$data['transactor'] = get_user_id();
			$data['transactor_name'] = get_user_name();
			$data['status'] = 1;

			$task_id = I(task_id);
			$list = M("TaskLog") -> add($data);
			if ($list != false) {
				$this -> _add_to_schedule($task_id);
				$return['info'] = '接受成功';
				$return['status'] = 1;
				$this -> ajaxReturn($return);
			} else {
				$this -> error('提交成功');
			}
		}
	}

	function accept() {
		if (IS_POST) {
			$task_log_id = I('task_log_id');
			$data['id'] = $task_log_id;
			$data['transactor'] = get_user_id();
			$data['transactor_name'] = get_user_name();
			$data['status'] = 1;
			$list = M("TaskLog") -> save($data);

			$task_id = M("TaskLog") -> where("id=$task_log_id") -> getField('task_id');
			M("Task") -> where("id=$task_id") -> setField('status', 1);

			if ($list != false) {
				$this -> _add_to_schedule($task_id);
				$return['info'] = '接受成功';
				$return['status'] = 1;
				$this -> ajaxReturn($return);
			} else {
				$this -> error('提交成功');
			}
		}
	}

	function reject() {
		$widget['editor'] = true;
		$this -> assign("widget", $widget);
		if (IS_POST) {
			$model = D("TaskLog");
			if (false === $model -> create()) {
				$this -> error($model -> getError());
			}
			$model -> transactor = get_user_id();
			$model -> transactor_name = get_user_name();
			$model -> finish_time = to_date(time());
			$list = $model -> save();
			$status = I('status');
			$task_id = I('task_id');
			if ($status > 2) {
				$where_count['task_id'] = array('eq', $task_id);
				$total_count = M("TaskLog") -> where($where_count) -> count();

				$where_count['status'] = array('gt', 2);
				$finish_count = M("TaskLog") -> where($where_count) -> count();
				if ($total_count == $finish_count) {
					M("Task") -> where("id=$task_id") -> setField('status', 5);
					$user_id = M('Task') -> where("id=$task_id") -> getField('user_id');

					$info = M("Task") -> where("id=$task_id") -> find();

					$transactor_name = get_user_name();

					$push_data['type'] = '任务';
					$push_data['action'] = '拒绝接受';
					$push_data['title'] = "{$transactor_name}拒绝接受您发起的[{$info['name']}]任务";
					$push_data['content'] = "如有问题，请与[{$transactor_name}]进行沟通。";
					$push_data['url']=U("Task/read?id={$info['id']}");		
								
					send_push($push_data, $user_id);
				}
			}
			if ($list !== false) {
				$this -> success('提交成功');
			} else {
				$this -> success('提交失败');
			}
		}

		$task_id = I('task_id');
		$where_log1['type'] = 1;
		$where_log1['executor'] = get_user_id();
		$where_log1['task_id'] = $task_id;
		$task_log1 = M("TaskLog") -> where($where_log1) -> find();
		if ($task_log1) {
			$this -> assign('task_log', $task_log1);
		} else {
			$where_log2['type'] = 2;
			$where_log2['executor'] = get_dept_id();
			$where_log2['task_id'] = $task_id;
			$task_log2 = M("TaskLog") -> where($where_log2) -> find();

			if ($task_log2) {
				$this -> assign('task_log', $task_log2);
			}
		}
		$this -> display();
	}

	public function save_log($id) {
		$model = D("TaskLog");
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}

		$model -> transactor = get_user_id();
		$model -> transactor_name = get_user_name();
		if ($status == 4) {
			$model -> finish_time = time();
		}

		$list = $model -> save();

		foreach ($_POST['jd_id'] as $key => $value) {
			$jd[$key] = array(
				'jd_ing'=>$_POST['jd_ing'][$key],
				'jd_jh'=>$_POST['jd_jh'][$key],
				'finish_rate_jd'=>$_POST['finish_rate_jd'][$key],
				'finish_jh_jd'=>$_POST['finish_jh_jd'][$key],
				);
			M('Task_jd')->where(array('id'=>$_POST['jd_id'][$key]))->save($jd[$key]);
		}

		$task_log_id = $id;
		$status = I('status');
		$task_id = M("TaskLog") -> where("id=$task_log_id") -> getField('task_id');

		if ($status == 2) {
			M("Task") -> where("id=$task_id") -> setField('status', 2);
		}

		if ($status == 4) {
			$task_id = I('task_id');
			$forword_executor = I('forword_executor');
			D('Task') -> forword($task_id, $forword_executor);
		}

		if ($status > 2) {
			$where_count['task_id'] = array('eq', $task_id);
			$total_count = M("TaskLog") -> where($where_count) -> count();

			$where_count['status'] = array('gt', 2);
			$finish_count = M("TaskLog") -> where($where_count) -> count();
			if ($total_count == $finish_count) {
				M("Task") -> where("id=$task_id") -> setField('status', 3);
				$user_id = M('Task') -> where("id=$task_id") -> getField('user_id');

				$info = M("Task") -> where("id=$task_id") -> find();

				$transactor_name = get_user_name();

				$push_data['type'] = '任务';
				$push_data['action'] = '已完成';
				$push_data['title'] = "{$transactor_name}已完成您发起的[{$info['name']}]任务";
				$push_data['content'] = "如有问题，请与[{$transactor_name}]进行沟通。";
				$push_data['url']=U("Task/read?id={$info['id']}");	
				
				send_push($push_data, $user_id);
			}
		}


		if ($list !== false) {
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('提交成功!');
			//成功提示
		} else {
			$this -> error('提交失败!');
			//错误提示
		}
	}

	function upload() {
		$this -> _upload();
	}

	function down($attach_id) {
		$this -> _down($attach_id);
	}

	private function _add_to_schedule($task_id) {
		$info = M("Task") -> where("id=$task_id") -> find();
		$data['name'] = $info['name'];
		$data['content'] = $info['content'];
		$data['start_time'] = to_date(time());
		$data['end_time'] = $info['expected_time'];
		$data['user_id'] = get_user_id();
		$data['user_name'] = get_user_name();
		$data['priority'] = 3;

		$list = M('Schedule') -> add($data);
	}

	function _send_mail_finish($task_id, $executor) {
		$executor_info = M("User") -> where("id=$executor") -> find();

		$email = $executor_info['email'];
		$user_name = $executor_info['name'];

		$info = M("Task") -> where("id=$task_id") -> find();

		$transactor_name = M("TaskLog") -> where("task_id=$task_id") -> getField('id,transactor_name');

		$transactor_name = implode(",", $transactor_name);

		$title = "您发布任务已完成：" . $info['name'];

		$body = "您好，{$user_name}，{$transactor_name} 完成了您发起的[{$info['name']}]任务</br>";
		$body .= "任务主题：{$info['name']}</br>";
		$body .= "任务时间：{$info['expected_time']}</br>";
		$body .= "任务执行人：{$transactor_name}</br>";
		$body .= "请及时检查任务执行情况，如有问题，请与[{$transactor_name}]进行沟通。</br>";
		$body .= "任务完成后请对[任务执行人]表达我们的感谢。</br>";

		$body .= "点击查看任务详情：http://" . $_SERVER['SERVER_NAME'] . U('Task/read', 'id=' . $info['id']) . "</br>";
		$body .= "霞湖世家，感谢有您！</br>";

		send_mail($email, $user_name, $title, $body);
	}

	public  function saveadd(){
		if($_POST){
        $user_id = get_user_id();
        $dept_id = M('User') -> where("id=$user_id") -> getField('dept_id');
        $user_name = M('User') -> where("id=$user_id") -> getField('name');
        $time = time();
		$data = array(
			'executor'=>$_POST['executor'],
			'add_file'=>$_POST['add_file'],
			'assign_user'=>$_POST['assign_user'],
			'name'=>$_POST['name'],
			'start_time'=>$_POST['start_time'],
			'expected_time'=>$_POST['expected_time'],
			'content'=>$_POST['content'],
			'user_id'=>$user_id,
			'user_name'=>$user_name,
			'dept_id'=>$dept_id,
			'is_del'=>0,
			'create_time'=> $time,
			'task_no'=>date('Y') . "-".date('d'),
			);
		$id = M("Task")->add($data);

		 foreach ($_POST['jd_name'] as $key => $value) {
				$jd[$key] = array(
					'tid'=>$id,
					'jd_name'=>$value,
					'start_time_jd'=>$_POST['start_time_jd'][$key],
					'expected_time_jd'=>$_POST['expected_time_jd'][$key],
					'is_del'=>0,
					);
				if($jd[$key]!=""){
					M("Task_jd")->add($jd[$key]);	
				}

			}	
		
		$executor_list = $_POST['executor'];
		$executor_list = array_filter(explode(';', $executor_list));
		if (!empty($executor_list)) {
			foreach ($executor_list as $key => $val) {
				$tmp = explode('|', $val);
				$executor_name = $tmp[0];
				$executor = $tmp[1];

				if (strpos($executor, "dept_") !== false) {
					$type = 2;
					$executor = str_replace('dept_', '', $executor);					
					$where['dept_id']=array('eq',$executor);					
					$dept_user_list=M('User')->where($where)->getField('id',true);

					foreach($dept_user_list as $val){
						$auth=D("Role")->get_auth('Task',$val);
						if($auth['admin']){
							$user_list[]=$val;
						}
					}
				} else {
					$type = 1;
					$user_list[] = $executor;
				}
				$log_data['executor'] = $executor;
				$log_data['executor_name'] = $executor_name;
				$log_data['type'] = $type;
				$log_data['assigner'] = $user_id;
				$log_data['task_id'] = $id;
				M("TaskLog") -> add($log_data);
			}
			
			$push_data['type'] = '任务';
			$push_data['action'] = '需要执行';
			$push_data['title'] = "来自：" . get_dept_name()."-".get_user_name();
			$push_data['content'] = "标题：" . $_POST['name'];
			$push_data['url'] = U("Task/read?id={$_POST['id']}");			
			send_push($push_data, $user_list);
		}
		}
		 $this -> success('增加成功');

	}

	public  function saveedit(){
		if($_POST){
		$data= array(
			'name'=>$_POST['name'],
			'executor'=>$_POST['executor'],
			'add_file'=>$_POST['add_file'],
			'assign_user'=>$_POST['assign_user'],
			'start_time'=>$_POST['start_time'],
			'expected_time'=>$_POST['expected_time'],
			'content'=>$_POST['content'],
			);

		M("Task")->where(array('id'=>$_POST['id']))->save($data);
        $task_jd = M("Task_jd")->where(array('tid'=>$_POST['id']))->select();
		foreach ($_POST['jd_name'] as $key => $value) {
				$jd[$key] = array(
					'tid'=>$_POST['id'],
					'jd_name'=>$value,
					'start_time_jd'=>$_POST['start_time_jd'][$key],
					'expected_time_jd'=>$_POST['expected_time_jd'][$key],
					'is_del'=>0,
					);
		M("Task_jd")->where(array('id'=>$task_jd[$key]['id']))->save($jd[$key]);
		}
    
		$user_id = get_user_id();
		$executor_list = $_POST['executor'];
		$executor_list = array_filter(explode(';', $executor_list));
		if (!empty($executor_list)) {
			foreach ($executor_list as $key => $val) {
				$tmp = explode('|', $val);
				$executor_name = $tmp[0];
				$executor[] = $tmp[1];
			}	
			$task_log = M("Task_log")->where(array('task_id'=>$_POST['id']))->select();
  		    foreach ($task_log as $key => $value) {
  			    $task_log[$key] = $value['executor'];
  		    }	
		
			$rtA = array();
			$rtB = array();
			$this->foo($task_log, $rtB);
			$this->foo($executor, $rtA);
			$del_id =array_diff($rtB, $rtA);
			if($del_id){
			foreach ($del_id as $key => $value) {
				$task_log = M("Task_log")->where(array('task_id'=>$_POST['id'],'executor'=>$value))->delete();
			}	
			}
        }
		$task_log = M("Task_log")->where(array('task_id'=>$_POST['id']))->select();
  		foreach ($task_log as $key => $value) {
  			$task_log[$key] = $value['executor'];
  		}
		if (!empty($executor_list)) {
			foreach ($executor_list as $key => $val) {
				$tmp = explode('|', $val);
				$executor_name = $tmp[0];
				$executor = $tmp[1];
				if(in_array($executor,$task_log)){
 					continue;
				}else{
		        if (strpos($executor, "dept_") !== false) {
					$type = 2;
					$executor = str_replace('dept_', '', $executor);					
					$where['dept_id']=array('eq',$executor);					
					$dept_user_list=M('User')->where($where)->getField('id',true);

					foreach($dept_user_list as $val){
						$auth=D("Role")->get_auth('Task',$val);
						if($auth['admin']){
							$user_list[]=$val;
						}
					}
				} else {
					$type = 1;
					$user_list[] = $executor;
				}
				$log_data['executor'] = $executor;
				$log_data['executor_name'] = $executor_name;
				$log_data['type'] = $type;
				$log_data['assigner'] = $user_id;
				$log_data['task_id'] = $_POST['id'];
				M("TaskLog") -> add($log_data);
				}		
			}		
		}
			
		}
      $this -> success('编辑成功');
	}
	public function jd_delete($id){
		if($id){
		  M("Task_jd")->where(array('id'=>$id))->delete();	
		   $this -> success('删除成功');
		}

	}
	public function foo($arr, &$rt) {
    if (is_array($arr)) {
        foreach ($arr as $v) {
            if (is_array($v)) {
                foo($v, $rt);
            } else {
                $rt[] = $v;
            }
        }
    }
    return $rt;
}
}
