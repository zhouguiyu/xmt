<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class EnterController extends HomeController {

	protected $config = array('app_type' => 'master');

    public function index() {
		$model = M("Enter");
        $where =array('is_del'=>0);       
		$list = $model -> where($where)-> select();
		foreach ($list as $key=>$val) {		
		   $room_name= M('Room') -> where(array('id'=>$val['room_id']))->find();		 	    		
		   $bulid_name= M('Building') -> where(array('id'=> $room_name['floor_id'])) -> find();	
		   $m_name= M('Building') -> where(array('id'=> $bulid_name['pid'])) -> find();	
		   $list[$key]['room_name']	 = $room_name['name'];	
           $list[$key]['build_name']= $bulid_name['name'];	
           $list[$key]['room_status'] =$room_name['status'];	
           $list[$key]['m_name']= $m_name['name'];	
		}
		$this -> assign('list', $list);
		$this -> display();
	}

	public function add($id,$svg,$floor_id) {
		$model = M("Enter");
		$list = $model -> where('is_del=0') -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_grade_list', $list);
		$this -> assign('id', $id);
        $this -> assign('svg', $svg);
        $this -> assign('floor_id', $floor_id);    
		$this -> display('add');
	}

	public	function del($id) {	
		$count = $this -> _del($id,enter,true);
		if ($count) {
			$model = D("SystemTag");			
			$result = $model -> del_data_by_row($id);
		}
		if ($count !== false) {//保存成功
			$this -> assign('jumpUrl', 'index.php?m=&c=Enter&a=index');
			$this -> success("成功删除{$count}条!");
		} else {
			//失败提示
			$this -> error('删除失败!');
		}
	}

	
	public function winpop() {		
		$node = M("Building");
		$menu = array();
		$menu = $node -> where('is_del=0') -> field('id,pid,name') -> order('sort asc') -> select();

		$tree = list_to_tree($menu);
		$this -> assign('menu', popup_tree_menu($tree));
		$pid = array();
		$this -> assign('pid', $pid);
		$this -> display();
	}

	public function winpop2() {
		$this -> winpop();
	}

}
?>