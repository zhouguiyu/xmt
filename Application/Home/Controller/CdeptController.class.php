<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class CdeptController extends HomeController {

	protected $config = array('app_type' => 'master');
	public function index() {
		$node = M("Cdept");
		$menu = array();
		//系统管里公司组织图显示当前登录部门管理       
		$menu = $node -> field('id,name,is_del')-> where(array('is_del'=>0)) -> order('sort asc') -> select();
		$this -> assign('menu', $menu);
		$this -> display();
	}

	public function add() { 
        $this -> display();
	}


	public function del($id) {
		$this -> _destory($id);
	}

	public function winpop() {
		$node = M("Dept");
		$menu = array();
		$menu = $node -> where('is_del=0') -> field('id,pid,name') -> order('sort asc') -> select();	
		$tree = list_to_tree($menu);	
		$this -> assign('menu', popup_tree_menu($tree[0]['_child']));
		$pid = array();
		$this -> assign('pid', $pid);
		$this -> display();
	}

	public function winpop2() {
		$this -> winpop();
	}



}
?>