<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class RoomController extends HomeController {
	protected $config = array('app_type' => 'master');
	public function index() {
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);

		$map = $this -> _search();		
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}
        $user_id = get_user_id();
       
		$model = D("Room");
        $map['is_del'] = array('eq', '0');

		if (!empty($model)) {
			$list = $this -> _list($model, $map);
		}		
		foreach ($list as $key => $value) {

            $Building =M('Building')-> where('id='.$value['floor_id']) -> getField('id,name,pid');
		   foreach ($Building as  $valueb) {
		  	  $Building2 =M('Building')-> where('id='.$valueb['pid']) -> getField('id,name');
		   }
           $list[$key]['floor_pname'] = $Building2[$Building[$value['floor_id']]['pid']];
           //房间到期时间戳
            $end  =strtotime($value['end_date']);
            //房间到期之前一周时间戳
            $endts = $end  - (7 * 24 * 60 * 60);
            $list[$key]['endts']= $endts;
		}
		$this -> assign('list', $list);
		$this -> display();
	}

    public  function  readsvg($id,$svg,$floor_id){
         $vo =M('Room')-> where('id='.$id) -> find();
         $this -> assign('id', $id);
         $this -> assign('svg', $svg);
         $this -> assign('floor_id', $floor_id);
         $image = M('Room_image')-> where(array('room_id'=>$id)) ->select();
         $this -> assign('image', $image);
         $this -> assign('vo', $vo);
         $this -> display();
     }

	public function add() {
		$plugin['date'] = true;
       
		$this -> assign("plugin", $plugin);
		$model = M("Room");
		$list = $model -> where('is_del=0') -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_grade_list', $list);
		$this -> display();
	}

	public function roomadd(){
		$dataroom = array(
			'name'=>$_POST['name'],
			'is_del'=>0,
			'short'=>$_POST['short'],
			'remark'=>$_POST['remark'],
			'status'=>$_POST['status'],
			'floor_name'=>$_POST['floor_name'],
			'floor_id'=>$_POST['floor_id'],
			'customer_id'=>$_POST['customer_id'],
			'start_date'=>$_POST['start_date'],
			'end_date'=>$_POST['end_date'],
			'customer_name'=>$_POST['customer_name'],
			'acreage'=>$_POST['acreage'],
			'sort'=>$_POST['sort'],
			'room_style'=>$_POST['room_style'],
			'wy_price'=>$_POST['wy_price'],
			'qn_price'=>$_POST['qn_price'],
			'fz_price'=>$_POST['fz_price'],
			);
        $room_id = M('Room') ->add($dataroom);
		foreach ($_POST['img'] as $key => $value) {
		    $dataiamge[$key] =array(
	    	'room_id'=>$room_id,
	    	'is_del'=>0,
	    	'image'=>$value,
	    	);
		    M('Room_image') ->add($dataiamge[$key]);

		}
   		$this -> success('添加成功');

	}

	public function saveroom(){
		$dataroom = array(
			'name'=>$_POST['name'],
			'is_del'=>0,
			'short'=>$_POST['short'],
			'remark'=>$_POST['remark'],
			'status'=>$_POST['status'],
			'floor_name'=>$_POST['floor_name'],
			'floor_id'=>$_POST['floor_id'],
			'customer_id'=>$_POST['customer_id'],
			'start_date'=>$_POST['start_date'],
			'end_date'=>$_POST['end_date'],
			'customer_name'=>$_POST['customer_name'],
			'acreage'=>$_POST['acreage'],
			'sort'=>$_POST['sort'],
			'room_style'=>$_POST['room_style'],
			'wy_price'=>$_POST['wy_price'],
			'qn_price'=>$_POST['qn_price'],
			'fz_price'=>$_POST['fz_price'],
			);
        M('Room') ->where(array('id'=>$_POST['id']))->save($dataroom);
        M('Room_image') ->where(array('room_id'=>$_POST['id']))->delete();
		foreach ($_POST['img'] as $key => $value) {
		    $dataiamge[$key] =array(
	    	'room_id'=>$_POST['id'],
	    	'is_del'=>0,
	    	'image'=>$value,
	    	);
		 M('Room_image') ->add($dataiamge[$key]);
		}
   		$this -> success('编辑成功');

	}
	public function del($id) {
		$this -> _destory($id);
	}

	public function winpop() {
		$node = M("Building");
		$menu = array();
        $user_id = get_user_id();
        $where =array();   
        $where =array('is_del'=>0);
		$menu = $node -> where($where) -> field('id,pid,name') -> order('sort asc') -> select();

		$tree = list_to_tree($menu);
		$this -> assign('menu', popup_tree_menu($tree));
		$pid = array();
		$this -> assign('pid', $pid);
		$this -> display();
	}

	public function winpop_customer() {
		$node = M("customer");
        $where =array('is_del'=>0);
		$menu = array();
		$menu = $node -> where($where) -> field('id,name') ->select();
		$tree = list_to_tree($menu);
		$this -> assign('menu', popup_tree_menu($tree));		
		$this -> display();
	}

	function edit($id) {
		$plugin['date'] = true;
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);
        $image = M('Room_image')-> where(array('room_id'=>$id)) ->select();
        $this -> assign("image", $image);
		$this -> _edit($id);
	}

}
?>