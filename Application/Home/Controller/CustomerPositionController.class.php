<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
--------------------------------------------------------------*/

namespace Home\Controller;

class CustomerPositionController extends HomeController {
	protected $config = array('app_type' => 'master');

	public function index($id) {
		$model = M("customer_position");
        $where['customer_id']= $id;
		$list = $model -> where($where)->order('sort') -> select();
		$this -> assign('list', $list);
		$this -> assign('id', $id);
		$this -> display();
	}

	function _search_filter(&$map) {
		$keyword = I('keyword');
		if (!empty($keyword)) {
			$map['position_no|name'] = array('like', "%" . $keyword . "%");
		}
	}

    function add($customer_id) {
        $this -> assign('customer_id', $customer_id);
        $this -> display();
    }

	function del() {
		$id = $_POST['id'];
		$this -> _destory($id);
	}

}
?>