<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
--------------------------------------------------------------*/

namespace Home\Controller;

class OpusController extends HomeController {
	protected $config = array('app_type' => 'common', 'admin' => 'opusdel,opususave,opusedit,opusread,opusave,read,opusadd,opus,set_tag,winpop,delnonation,tag_manage,customer_dept,add_dept,customer_position,customer_deptgrade,customer_user');

	public function index($id){
		$list = M('Opus')->where(array('customer_id'=>$id,'is_del'=>0))->select();
        $this -> assign("list", $list);
		$this->display();
	}
	public function add(){
		$this->display();
	}
	public function opusave(){
		$data = array(
			'name'=>$_POST['name'],
			'hjname'=>$_POST['hjname'],
			'sc'=>$_POST['sc'],
			'customer_id'=>$_POST['customer_id'],
			'customer_name'=>$_POST['customer_name'],
			);
		M('Opus')->add($data);
		$this -> assign('jumpUrl', get_return_url());
		$this -> success('增加成功！');

	}
	public function read($id){
		$vo = M('Opus')->where(array('id'=>$id))->select();
        $this -> assign("vo", $vo);
		$this->display();	
	}
	public function opusedit($id){
		$vo = M('Opus')->where(array('id'=>$id))->find();
        $this -> assign("vo", $vo);
		$this->display();	
	}
	public function opususave(){
		$data = array(
			'name'=>$_POST['name'],
			'hjname'=>$_POST['hjname'],
			'sc'=>$_POST['sc'],
			'customer_id'=>$_POST['customer_id'],
			'customer_name'=>$_POST['customer_name'],
			);
		M('Opus')->where(array('id'=>$_POST['id']))->save($data);
		$this -> assign('jumpUrl', get_return_url());
		$this -> success('编辑成功！');
	}

	function opusdel($id) {
		$count = $this -> _del($id, Opus, true);
		if ($count) {
			$model = D("SystemTag");
			$result = $model -> del_data_by_row($id);
		}

		if ($count !== false) {//保存成功
			$this -> assign('jumpUrl', get_return_url());
			$this -> success("成功删除{$count}条!");
		} else {
			//失败提示
			$this -> error('删除失败!');
		}
	}

}
?>