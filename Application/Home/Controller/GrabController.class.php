<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
--------------------------------------------------------------*/

namespace Home\Controller;

class GrabController extends HomeController {

	protected $config = array('app_type' => 'folder,info_read,customer_info_read', 'read' => 'grabread,grablist,grab,zxdel,zxupdata,zx,zxedit,addzx,savezx,info_list,saveinfolist,my_sign,my_info,sign_info,folder,sign,info_read', 'admin' => 'mark,move_to,folder_manage,customer_info_read,scope,actor', 'write' => 'sign_report,upload');

	//过滤查询字段
	function _search_filter(&$map) {
		$map['is_del'] = array('eq', '0');
		$keyword = I('keyword');
		if (!empty($keyword) && empty($map['name'])) {
			$map['name'] = array('like', "%" . $keyword . "%");
		}
	}

	public function index() {

		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);

		$unread_info = $this -> _unread_info();
		$this -> assign("unread_info", $unread_info);

		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}	
	    $map['is_del'] =0;
		$model = M("Grab");
		if (!empty($model)) {
			$this -> _list($model, $map, 'id desc');
		}
		$this -> display();
	}

	public function my_info() {

		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);

		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		$user_id = get_user_id();
		$map['user_id'] = array('eq', $user_id);

		$model = D("InfoView");
		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> display();
	}

	public function my_sign() {

		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);

		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		$user_id = get_user_id();
		$where['user_id'] = array('eq', $user_id);
		$sign_list = M("InfoSign") -> where($where) -> getField('info_id', true);

		if ($sign_list) {
			$map['id'] = array('in', $sign_list);

			$model = D("InfoView");
			if (!empty($model)) {
				$this -> _list($model, $map);
			}
		}
		$this -> display();
	}

	public	function del($id) {	
		$count = $this -> _del($id, Grab, true);
		if ($count) {
			$model = D("SystemTag");			
			$result = $model -> del_data_by_row($id);
		}
		if ($count !== false) {//保存成功
			$this -> assign('jumpUrl',  get_return_url());
			$this -> success("成功删除{$count}条!");
		} else {
			//失败提示
			$this -> error('删除失败!');
		}
	}

	public function move_to($id, $val) {
		$where['id'] = array('in', $id);
		$folder = M("Info") -> distinct(true) -> where($where) -> field("folder") -> select();
		if (count($folder) == 1) {
			$auth = D("SystemFolder") -> get_folder_auth($folder[0]["folder"]);
			if ($auth['admin'] == true) {
				$field = 'folder';
				$this -> _set_field($id, $field, $val);
			}
			$return['info'] = '操作成功';
			$return['status'] = 1;
			$this -> ajaxReturn($return);
		} else {
			$return['info'] = '操作成功';
			$return['status'] = 1;
			$this -> ajaxReturn($return);
		}
	}

	function sign($id) {
		$user_id = get_user_id();

		$model = M("Info");
		$folder_id = $model -> where("id=$id") -> getField('folder');

		$Form = D('InfoSign');
		$data['info_id'] = $id;
		$data['user_id'] = $user_id;
		$data['folder'] = $folder_id;
		$data['user_name'] = get_user_name();
		$data['dept_id'] = get_dept_id();
		$data['dept_name'] = get_dept_name();

		$data['is_sign'] = '1';
		$data['sign_time'] = time();
		$result = $Form -> add($data);
		if ($result) {
			$this -> _readed($id);
			$return['status'] = 1;
			$return['info'] = "签收成功";
			$this -> ajaxReturn($return);
		} else {
			$return['status'] = 0;
			$return['info'] = "签收失败";
			$this -> ajaxReturn($return);
		}
	}

	function sign_info($id) {

		$row_info = M('Info') -> find($id);
		$this -> assign('row_info', $row_info);

		$model = M("InfoSign");
		$where['info_id'] = array('eq', $id);
		$where['user_id'] = array('eq', get_user_id());
		$list = $model -> where($where) -> find();
		$this -> assign('vo', $list);
		$this -> display();
	}

	function sign_report($id) {

		$row_info = M("Info") -> find($id);
		//dump($row_info);
		$this -> assign('row_info', $row_info);

		//签收人员
		$signed_user = M("InfoSign") -> where("info_id=$id") -> getField('user_id', true);

		//发布范围
		$sign_time = M("InfoSign") -> where("info_id=$id") -> getField('user_id,sign_time');
		$this->assign('sign_time',$sign_time);
		
		//发布范围
		$actor_user = M("InfoScope") -> where("info_id=$id") -> getField('user_id', true);

		//未签收人员
		if (!empty($signed_user)) {
			$un_sign_user = array_diff($actor_user, $signed_user);
		} else {
			$un_sign_user = $actor_user;
		}

		$model = D("UserView");
		if (!empty($signed_user)) {
			$where_signed['id'] = array('in', $signed_user);
			$signed_user_info = $model -> where($where_signed) -> select();
			$this -> assign('signed_user_info', $signed_user_info);
		}

		if (!empty($un_sign_user)) {
			$where_un_sign['id'] = array('in', $un_sign_user);
			$un_sign_user_info = $model -> where($where_un_sign) -> select();
			$this -> assign('un_sign_user_info', $un_sign_user_info);
		}

		$this -> display();
	}

	public function add() {		
		$this -> display();
	}
	


	public function edit($id) {
		$plugin['uploader'] = true;
		$plugin['editor'] = true;		
		$this -> assign("plugin", $plugin);
		$this -> _edit($id);
	}

	public function zxdel($id){
        M('Project_notation')->where(array('id'=>$id))->delete();
	}

	public function read($id) {
		$model = M('Grab');
		$vo = $model -> find($id);
		$this -> assign('vo', $vo);
		$this -> display();
	}

    public function grab($id) {  
    	$model = M('Grab');
		$name = $model -> where(array('id'=>$id))->getField('name');  
     	//关键词url	     	
		$bdurl ="http://www.baidu.com/baidu?&ie=utf-8&word=".$name;
		$strContent = file_get_contents($bdurl);
        preg_match_all("/.* data-tools=([^>]+)>/U",$strContent, $out);
		//结果url
		$url = array();
		//结果url数组
	    foreach ($out[1] as $key => $rows){
            $url[] = substr($rows,strpos($rows, "url")+5,'-3');
           if(substr($url[$key], 0, 1 )!='h'){
           	  $length = strlen($url[$key]);
              $url[$key]=  substr($url[$key],1,($length-1));
           }           
	   }

	   foreach ($url as $keyu => $value) {	   	 
	        $urllink = $this->getrealurl($value); 
	        $html=substr($urllink,-4); 	       
	        if($html!='html') {
	        	unset($url[$keyu]);
	        }
	   }	   
	   //取数据库中取当前关键词的url
	   $murl=  M('Grab_url')->where(array('gid'=>$id))->select(); 
	   foreach ($murl as $key => $value) {
	     $murl[$key] = $value['url'];
	   }
	    //合并两个数组
		if(is_array($murl)){
		   $urllist = array_merge($url,$murl);
		}else{
			$urllist= $url;
		}	  
	    // 去除相同的url 
		$temp = array_unique($url); 
		//url存入数据库	
		M('Grab_url')->where(array('gid'=>$id))->delete();	
		import('@.ORG.Util.Grab');
    	foreach ($temp as $keys => $values){     		
            $grab[$keys]=array(
			'gid'=>$id,
			'url'=>$values,
			'is_del'=>0,
          	);  
	        M('Grab_url')->add($grab[$keys]);	      
	        $urllink = $values;	        
	     	//$Content = file_get_contents($urllink); 
	     	$Content = $this->auto_read($urllink);    	
	     	if(isset($Content) && !empty($Content)){
	     		$read = new \Readability($Content);  		     	   
	     	  	$tcontent[$keys] =$read->getContent();
		        if($tcontent[$keys]['title']!='' && $tcontent[$keys]['content']!=''){
		        $data[$keys] =array(
		            'gid'=>$id,			
					'is_del'=>0,
					'title'=>$tcontent[$keys]['title'],
					'content'=>$tcontent[$keys]['content'],		
					'url'=>$this->getrealurl($values),						
		         	);	        	
		        }	
	     	}else{
	     		continue;
	     	}		
		       
    	} 
    	M('Grab_content')->where(array('gid'=>$id))->delete();	
    	foreach ($data as $keya => $value) {
    		 $arr[$keya] =array(
	            'gid'=>$id,			
				'is_del'=>0,
				'title'=> $value['title'],
				'content'=> $value['content'],
				'url'=> $value['url'],
	         	);
    		M('Grab_content')->add($arr[$keya]);  
    	}
        $arr = array ('status'=>1);
        echo json_encode($arr);
    }

	function auto_read($file, $charset='UTF-8') {
	    $list = array('GBK', 'UTF-8');
	     $str = file_get_contents($file);
	     foreach ($list as $item) {
	        $tmp = mb_convert_encoding($str, $item, $item);
	        if (md5($tmp) == md5($str)) {
	             return mb_convert_encoding($str, $charset, $item);
	         }
	     }
	    return $str;
	}
	 public function getrealurl($url){
	 	$header = get_headers($url,1);	 
		if (strpos($header[0],'301') || strpos($header[0],'302')) {
			if(is_array($header['Location'])) {
				return $header['Location'][count($header['Location'])-1];
			}else{
				return $header['Location'];
			}
		}else {
			return $url;
		}
	}


	public function grablist($id){		
	  $list =  M('Grab_content')->where(array('gid'=>$id))->select();
	 // print_r($list);exit;
	  $this -> assign('list', $list);
	  $this -> display();
	}

	public function grabread($id){
		  $list =  M('Grab_content')->where(array('id'=>$id))->find();
		  $this -> assign('list', $list);
		  $this -> display();	
	}

	public function zx($id) {
		$model = M('Project_notation');
		$list = $model -> where(array('project_id'=>$id))->order('create_time desc')->select();
		$this -> assign('id', $id);
		$this -> assign('list', $list);
		$this -> display();
	}

// public function gzdecode($data) {

//   $len = strlen($data);
//   if ($len < 18 || strcmp(substr($data,0,2),"\x1f\x8b")) {
//     return null;  // Not GZIP format (See RFC 1952)
//   }
//   $method = ord(substr($data,2,1));  // Compression method
//   $flags  = ord(substr($data,3,1));  // Flags
//   if ($flags & 31 != $flags) {
//     // Reserved bits are set -- NOT ALLOWED by RFC 1952
//     return null;
//   }

//   // NOTE: $mtime may be negative (PHP integer limitations)

//   $mtime = unpack("V", substr($data,4,4));
//   $mtime = $mtime[1];
//   $xfl   = substr($data,8,1);
//   $os    = substr($data,8,1);
//   $headerlen = 10;
//   $extralen  = 0;
//   $extra     = "";
//   if ($flags & 4) {
//     // 2-byte length prefixed EXTRA data in header
//     if ($len - $headerlen - 2 < 8) {
//       return false;    // Invalid format

//     }
//     $extralen = unpack("v",substr($data,8,2));
//     $extralen = $extralen[1];
//     if ($len - $headerlen - 2 - $extralen < 8) {
//       return false;    // Invalid format
//     }
//     $extra = substr($data,10,$extralen);
//     $headerlen += 2 + $extralen;
//   }
 
//   $filenamelen = 0;
//   $filename = "";
//   if ($flags & 8) {
//     // C-style string file NAME data in header
//     if ($len - $headerlen - 1 < 8) {
//       return false;    // Invalid format
//     }
//     $filenamelen = strpos(substr($data,8+$extralen),chr(0));
//     if ($filenamelen === false || $len - $headerlen - $filenamelen - 1 < 8) {
//       return false;    // Invalid format
//     }
//     $filename = substr($data,$headerlen,$filenamelen);
//     $headerlen += $filenamelen + 1;
//   }
 
//   $commentlen = 0;
//   $comment = "";
//   if ($flags & 16) {
//     // C-style string COMMENT data in header
//     if ($len - $headerlen - 1 < 8) {
//       return false;    // Invalid format
//     }
//     $commentlen = strpos(substr($data,8+$extralen+$filenamelen),chr(0));
//     if ($commentlen === false || $len - $headerlen - $commentlen - 1 < 8) {
//       return false;    // Invalid header format
//     }
//     $comment = substr($data,$headerlen,$commentlen);
//     $headerlen += $commentlen + 1;
//   }
 
//   $headercrc = "";
//   if ($flags & 1) {
//     // 2-bytes (lowest order) of CRC32 on header present
//     if ($len - $headerlen - 2 < 8) {
//       return false;    // Invalid format
//     }
//     $calccrc = crc32(substr($data,0,$headerlen)) & 0xffff;
//     $headercrc = unpack("v", substr($data,$headerlen,2));
//     $headercrc = $headercrc[1];
//     if ($headercrc != $calccrc) {
//       return false;    // Bad header CRC
//     }
//     $headerlen += 2;
//   }
 
//   // GZIP FOOTER - These be negative due to PHP's limitations
//   $datacrc = unpack("V",substr($data,-8,4));
//   $datacrc = $datacrc[1];
//   $isize = unpack("V",substr($data,-4));
//   $isize = $isize[1];
 
//   // Perform the decompression:
//   $bodylen = $len-$headerlen-8;
//   if ($bodylen < 1) {
//     // This should never happen - IMPLEMENTATION BUG!
//     return null;
//   }
//   $body = substr($data,$headerlen,$bodylen);

//   $data = "";
//   if ($bodylen > 0) {
//     switch ($method) {
//       case 8:
//         // Currently the only supported compression method:
//         $data = gzinflate($body);
//         break;
//       default:
//         // Unknown compression method
//         return false;
//     }
//   } else {
//     // I'm not sure if zero-byte body content is allowed.
//     // Allow it for now...  Do nothing...
//   }
 
//   // Verifiy decompressed size and CRC32:
//   // NOTE: This may fail with large data sizes depending on how
//   //       PHP's integer limitations affect strlen() since $isize
//   //       may be negative for large sizes.
//   if ($isize != strlen($data) || crc32($data) != $datacrc) {
//     // Bad format!  Length or CRC doesn't match!
//     return false;
//   }
//   return $data;
// }

   //针对群发收件人进行 反馈审核
    public function info_list($id,$s){
    	//$s为需要修改的userid
       	$s = substr($s, 0, -1);
        $s = explode(",",$s );
        //当前通知下所有userid
        $infoall= M('Info_list')->where(array('infoid'=>$id))->select();
        //取相同的部分
        $info = array_diff($s, $infoall);
        foreach ($info  as $value) {
        	$where = array('user_id' => $value,'infoid'=>$id );
        	$data['status']=1;
        	M('Info_list')->where($where)->save($data);
        }
        echo json_encode(array('status'=>'1'));
    }

    //增加通知时同时向info_list表插入 user数据
	public function saveinfolist(){
		M('Info_list')->add($_POST);
	}

	public function folder($fid) {
		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);
		$this -> assign('auth', $this -> config['auth']);

		$this -> assign('fid', $fid);

		$unread_info = $this -> _unread_info();
		$this -> assign("unread_info", $unread_info);

		$model = D("InfoView");
		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		$map['folder'] = array('eq', $fid);

		$dept_id = get_dept_id();
		$map['_string'] = " Info.is_public=1 or Info.dept_id=$dept_id ";

		$user_id = get_user_id();
		$where_scope['user_id'] = array('eq', $user_id);
		$scope_list = M("InfoScope") -> where($where_scope) -> getField('info_id', true);
		$scope_list = implode(",", $scope_list);

		if (!empty($scope_list)) {
			$map['_string'] .= "or Info.id in ($scope_list)";
		}

		if (!empty($model)) {
			$this -> _list($model, $map, 'id desc');
		}

		$this -> assign("folder_name", D("SystemFolder") -> get_folder_name($fid));
		$this -> _assign_folder_list();
		$this -> display();
	}



	public function folder_manage() {
		$this -> _system_folder_manage("信息管理");
	}

	public function upload() {
		$this -> _upload();
	}

	function down($attach_id) {
		$this -> _down($attach_id);
	}

	private function _unread_info() {

		$map['is_del'] = array('eq', '0');
		$map['create_time'] = array("egt", time() - 3600 * 24 * 30);

		$user_id = get_user_id();
		$where_scope['user_id'] = array('eq', $user_id);
		$scope_list = M("InfoScope") -> where($where_scope) -> getField('info_id', TRUE);

		if (!empty($scope_list)) {
			$map['id'] = array('in', $scope_list);
		} else {
			$map['_string'] = " 1=2";
		}

		$model = D("InfoView");
		$info_list = $model -> where($map) -> getField('id', true);

		$readed_info = M("UserConfig") -> where("id=$user_id") -> getField('readed_info');
		$readed_info = array_filter(explode(',', $readed_info));

		if (!empty($info_list)) {
			$un_read_doc = array_diff($info_list, $readed_info);
		} else {
			$un_read_doc = array();
		}
		return $un_read_doc;
	}

	private function _readed($id) {
		$user_id = get_user_id();
		$folder_list = D("SystemFolder") -> get_authed_folder();

		//$where_readed['folder'] = array("in", $folder_list);
		//$where_readed['create_time'] = array("egt", time() - 3600 * 24 * 30);

		$readed_list = array_filter(explode(",", get_user_config("readed_info") . "," . $id));

	    //$where_readed['id'] = array('in', $readed_list);
        //print_r($where_readed);exit;
		$readed_info = M("Info") -> where($where_readed) -> getField("id", true);
		$readed_info = implode(",", $readed_info);

		$where_config['id'] = array('eq', $user_id);
		if (!empty($readed_info)) {
			M("UserConfig") -> where($where_config) -> setField('readed_info', $readed_info);
		}
	}
	function scope() {
		$this -> actor();
	}

	function actor() {
		$plugin['jquery-ui'] = true;
		$this -> _dept_list();		
		$this -> assign('type', 'dept');
		$this -> display();
		return;
	}

	private function _dept_list() {
		$model = M("cdept");
		$list = array();		
		$where['is_del']='0';
        $where['pid']=0;
		$list = $model -> where($where) -> field('id,name') -> order('sort asc') -> select();
		$list = list_to_tree($list);
	
		$this -> assign('list_dept', popup_tree_menu($list));
	}

	public function info_read($id) {
       	$type = I('type');
        $name = M("Cdept") -> where(array(id=>$id)) -> field(name) -> find();
        switch ($type) {
			case "dept" :			
				$modeld = M("customer_user");
                $where['dept_name'] = $name['name'];
				$data = $modeld -> where($where) ->select();
				break;
			default :
		}
		$new = array();
		$return['status'] = 1;
		$return['data'] = $data;
		$this -> ajaxReturn($return);
	}

}
