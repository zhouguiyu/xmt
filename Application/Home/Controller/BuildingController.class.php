<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class BuildingController extends HomeController {

	protected $config = array('app_type' => 'master');

	public function index() {
        $user_id = get_user_id();
        $where =array();  
        $where_user['is_del'] = array('eq', 0);
        $where_dept['is_del'] = array('eq', 0);
        $where_dept['pid'] = array('eq', 0);
        $where_file['is_del'] = array('eq', 0);
        $where_noenter['is_del'] = array('eq',0);
        $where_noenter['status'] = array('eq',1);
        $where_bl['is_del'] = array('eq', 0);
        $where_bl['status'] = array('eq',0);       
		$user_count = M("User") -> where($where_user) -> count();
		$this -> assign('user_count', $user_count);
        //楼宇数量
		$dept_count = M("Building") -> where($where_dept) -> count();
		$this -> assign('dept_count', $dept_count);
		//办公室总数量
		$file_count = M("Room") -> where($where_file) -> count();
		$this -> assign('file_count', $file_count);
		//入驻办公室总数量
		$noenter = M("Room") -> where($where_noenter) -> count();
        $this -> assign('noenter', $noenter);
        //空闲办公室总数量
        $bl = M("Room") -> where($where_bl) -> count();
		$this -> assign('bl', $bl);
		$this -> display();
		
	}
	public function floor() {
        $user_id = get_user_id();
       
		$node = M("Building");
		$menu = array();
		$menu = $node -> where($where)->field('id,pid,name,is_del') -> order('sort asc') -> select();
		$tree = list_to_tree($menu);
		$this -> assign('menu', popup_tree_menu($tree));

		$model = M("Building");
		$list = $model -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_list', $list);

		$model = M("DeptGrade");
		$list = $model -> where('is_del=0') -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_grade_list', $list);

		$this -> display();
		}

    public function room($id,$svg) {
        $where = array('is_del'=>0,'floor_id'=>$id);
        $room = M('Room') -> where($where) -> select();

        $kxroom = M('Room') -> where( array('is_del'=>0,'floor_id'=>$id,'status'=>'0')) -> select();
        $rzroom = M('Room') -> where( array('is_del'=>0,'floor_id'=>$id,'status'=>'1')) -> select();
        $blroom = M('Room') -> where( array('is_del'=>0,'floor_id'=>$id,'status'=>'2')) -> select();
        $ycroom = M('Room') -> where( array('is_del'=>0,'floor_id'=>$id,'status'=>'3')) -> select();
        //房间总数量
        $roomnum = count($room);
        $kxroomnum = count($kxroom);
        $rzroomnum = count($rzroom);
        $blroomnum = count($blroom);
        $ycroomnum = count($ycroom);
        $this -> assign('roomnum',$roomnum);
        $this -> assign('kxroomnum',$kxroomnum);
        $this -> assign('rzroomnum',$rzroomnum);
        $this -> assign('blroomnum',$blroomnum);
        $this -> assign('ycroomnum',$ycroomnum);

        foreach ($room as $key => $value) {
          $mianji = $value['acreage'];
          $fz_price = $value['fz_price'];  
          $qn_price = $value['qn_price']; 
          $wy_price = $value['wy_price'];  
          if($value['room_style']==0){
              //扶持 只收采暖 和物业 
            $price =($qn_price+$wy_price)*$mianji;
          }else{
            $price =($fz_price+$wy_price)*$mianji;
              //收取房租跟物业
         }
         $room[$key]['price']= $price;
        }
        $this -> assign('svg',$svg);
        $this -> assign('room',$room);
        $this -> assign('floor_id',$id);
        $this -> assign('num',count($room));
        $this -> display();
    }

    public function roomlist() {
        $user_id = get_user_id();
        $where =array();  
        $node = M("Building");
        $menu = array();
        $menu = $node -> where($where)->field('id,pid,name,is_del,svg') -> order('sort asc') -> select();
        $tree = list_to_tree($menu);
        $this -> assign('menu', popup_tree_floor($tree));

        $model = M("Building");
        $list = $model -> order('sort asc') -> getField('id,name');
        $this -> assign('dept_list', $list);

        $model = M("DeptGrade");
        $list = $model -> where('is_del=0') -> order('sort asc') -> getField('id,name');
        $this -> assign('dept_grade_list', $list);

        $this -> display();
    }
	public function add() {    
		$model = M("Building");
		$list = $model -> where('is_del=0') -> order('sort asc') -> getField('id,name');
		$this -> assign('dept_grade_list', $list);

		$this -> display();
	}
    

	public function del($id) {
		$this -> _destory($id);
	}

	public function winpop() {
        $user_id = get_user_id();
        $where =array();        
        $where =array('id_del'=>0);
		$node = M("Building");
		$menu = array();
		$menu = $node -> where($where) -> field('id,pid,name') -> order('sort asc') -> select();

		$tree = list_to_tree($menu);
		$this -> assign('menu', popup_tree_menu($tree));
		$pid = array();
		$this -> assign('pid', $pid);
		$this -> display();
	}

	public function winpop2() {
		$this -> winpop();
	}

}
?>