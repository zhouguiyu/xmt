<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
--------------------------------------------------------------*/

namespace Home\Controller;

class CustomerController extends HomeController {
	protected $config = array('app_type' => 'common', 'admin' => 'opusdel,opususave,opusedit,opusread,opusave,opusadd,opus,set_tag,winpop,delnonation,tag_manage,customer_dept,add_dept,customer_position,customer_deptgrade,customer_user');

	//过滤查询字段
	function _search_filter(&$map) {
		$map['user_id'] = array('eq', get_user_id());
		$map['is_del'] = array('eq', '0');
		if (!empty($_POST['tag'])) {
			$map['tag'] = $_POST['tag'];
		}
		$keyword = I('keyword');
		if (!empty($keyword)) {
			$where['name'] = array('like', "%" . $keyword . "%");
			$where['office_tel'] = array('like', "%" . $keyword . "%");
			$where['office_tel'] = array('like', "%" . $keyword . "%");
			$where['_logic'] = 'or';
			$map['_complex'] = $where;
		}
	}

	function index() {
		$model = M("Customer");
	    $user_id = get_user_id();		
		$is_admin = M('User')->where(array('id'=>$user_id))->getField('is_admin');
		if($is_admin=='1'){
			$customer_id = M('User')->where(array('id'=>$user_id))->getField('customer_id');
			$where['id'] =$customer_id;
		}
		$where['is_del'] = 0;
		$list = $model -> where($where) -> select();
		$this -> assign('list', $list);
		$tag_data = D("SystemTag") -> get_data_list();

		$new = array();
		foreach ($tag_data as $val) {
			if(!empty($new[$val['row_id']])){
				$new[$val['row_id']] = $new[$val['row_id']] . $val['tag_id'] . ",";	
			}else{
				$new[$val['row_id']] = $val['tag_id'] . ",";
			}			
		}
		$this -> assign('tag_data', $new);
		$this -> _assign_tag_list();
		$this -> display();
		return;
	}

	function export() {
		$model = M("Customer");
		$where['is_del'] = 0;
		$list = $model -> where($where) -> select();

		Vendor('Excel.PHPExcel');
		//导入thinkphp第三方类库

		// $inputFileName = "Public/templete/customer.xlsx";
		// $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel -> getProperties() -> setCreator("smeoa") -> setLastModifiedBy("smeoa") -> setTitle("Office 2007 XLSX Test Document") -> setSubject("Office 2007 XLSX Test Document") -> setDescription("Test document for Office 2007 XLSX, generated using PHP classes.") -> setKeywords("office 2007 openxml php") -> setCategory("Test result file");
		// Add some data

        $i = 1;
        $objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("A$i",'企业名称') -> setCellValue("B$i",'企业简称') -> setCellValue("C$i",'证件号码') ->  setCellValue("D$i", '地址') ->  setCellValue("E$i",'法人') -> setCellValue("F$i", '邮箱') -> setCellValue("G$i", '付款方式') -> setCellValue("H$i", '办公电话') -> setCellValue("I$i",'手机') -> setCellValue("J$i", '传真');
        //dump($list);
		foreach ($list as $val) {
			$i++;
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("A$i", $val["name"]) -> setCellValue("B$i", $val["short"]) -> setCellValue("C$i", $val["biz_license"]) -> setCellValue("D$i", $val["address"]) -> setCellValue("E$i", $val["contact"]) -> setCellValue("F$i", $val["email"]) ->setCellValue("G$i", $val["payment"]) -> setCellValue("H$i", $val["office_tel"]) -> setCellValue("I$i", $val["mobile_tel"]) -> setCellValue("J$i", $val["fax"]);
		}
		// Rename worksheet
		$objPHPExcel -> getActiveSheet() -> setTitle('Customer');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel -> setActiveSheetIndex(0);
		$file_name = "customer.xlsx";
		// Redirect output to a client’s web browser (Excel2007)
		header("Content-Type: application/force-download");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition:attachment;filename =" . str_ireplace('+', '%20', URLEncode($file_name)));
		header('Cache-Control: max-age=0');

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter -> save('php://output');
		exit ;
	}

	function read($id){
		$vo = M('Customer') -> where("id=$id") -> select();
	    foreach ($vo as $key => $value) {
			$user = M('User') -> where(array('id' => $value['user_id'], )) -> getField('name');
			$vo[$key]['user_name'] =$user;
		}
        $notation = M('customer_notation') -> where("customer_id=$id") -> select();
        $this -> assign("notation",$notation);
		$this -> assign('vo', $vo);
		$this -> display();

	}

	public function import() {
		$opmode = $_POST["opmode"];
		if ($opmode == "import") {
			$File = D('File');
			$file_driver = C('DOWNLOAD_UPLOAD_DRIVER');
			$info = $File -> upload($_FILES, C('DOWNLOAD_UPLOAD'), C('DOWNLOAD_UPLOAD_DRIVER'), C("UPLOAD_{$file_driver}_CONFIG"));

			if (!$info) {
				$this -> error($File -> getError());
			} else {
				//取得成功上传的文件信息
				Vendor('Excel.PHPExcel');
				//导入thinkphp第三方类库
				$inputFileName = C('DOWNLOAD_UPLOAD.rootPath') . $info['uploadfile']["savepath"] . $info['uploadfile']["savename"];

				$objPHPExcel = \PHPExcel_IOFactory::load($inputFileName);
				$sheetData = $objPHPExcel -> getActiveSheet() -> toArray(null, true, true, true);
				$model = M("Customer");
                $user_id = get_user_id();
				for ($i = 2; $i <= count($sheetData); $i++) {
					$data = array();
					$data['name'] = $sheetData[$i]["A"];
					$data['short'] = $sheetData[$i]["B"];
					$data['biz_license'] = $sheetData[$i]["C"];
					$data['address'] = $sheetData[$i]["D"];
					$data['contact'] = $sheetData[$i]["E"];
					$data['email'] = $sheetData[$i]["F"];
					$data['payment'] = $sheetData[$i]["G"];
					$data['office_tel'] = $sheetData[$i]["H"];
					$data['mobile_tel'] = $sheetData[$i]["I"];
					$data['fax'] = $sheetData[$i]["J"];
					$data['is_del'] = 0;
                    $data['status'] = 0;
                    $data['user_id']= $user_id;

					$model -> add($data);
				}
				//dump($sheetData);
				if (file_exists(__ROOT__ . "/" . $inputFileName)) {
					unlink(__ROOT__ . "/" . $inputFileName);
				}
				$this -> assign('jumpUrl', get_return_url());
				$this -> success('导入成功！');
			}
		} else {
			$this -> display();
		}
	}

	 function del($id) {
		$count = $this -> _del($id, CONTROLLER_NAME, true);
		if ($count) {
			$model = D("SystemTag");
			$result = $model -> del_data_by_row($id);
		}

		if ($count !== false) {//保存成功
			$this -> assign('jumpUrl', get_return_url());
			$this -> success("成功删除{$count}条!");
		} else {
			//失败提示
			$this -> error('删除失败!');
		}
	}

	function add() {
		$user_id = get_user_id();	
		$this -> assign('user_id', $user_id);	
		$this -> display();
	}

	function edit($id) {
		$user_id = get_user_id();
        $user_name = M('User') -> where("id=$user_id") -> getField('name');
        $this -> assign('user_name', $user_name);           
		$vo = M('Customer') -> where("id=$id") -> find();
		$this -> assign('vo', $vo);
		$this -> assign('id', $id);
		$this -> assign('user_id', $user_id);
		$this -> display();
	}

   public  function delnonation($type,$id,$customer_id){
       if($type==0){
           M('customer_notation')->where(array('id'=>$id))->delete();
       }else{
           $data =array(
               'remark'=>''
           );
           M('customer')->where(array('id'=>$customer_id))->save($data);
       }
       $this -> success('删除成功!','/index.php?m=&c=Customer&a=read&id='.$customer_id);

   }

	function set_tag() {
		$id = $_POST['id'];
		$tag = $_POST['tag'];
		$new_tag = $_POST['new_tag'];
		if (!empty($id)) {
			$model = D("SystemTag");
			$model -> del_data_by_row($id);
			if (!empty($_POST['tag'])) {
				$result = $model -> set_tag($id, $tag);
			}
		};

		if (!empty($new_tag)) {
			$model = D("SystemTag");
			$model -> controller = CONTROLLER_NAME;
			$model -> name = $new_tag;
			$model -> is_del = 0;
			$model -> user_id = get_user_id();
			$new_tag_id = $model -> add();
			if (!empty($id)) {
				$result = $model -> set_tag($id, $new_tag_id);
			}
		};
		if ($result !== false) {//保存成功
			if ($ajax || IS_AJAX)
				$this -> assign('jumpUrl', get_return_url());
			$this -> success('操作成功!');
		} else {
			//失败提示
			$this -> error('操作失败!');
		}
	}

	function tag_manage() {
		$this -> _system_tag_manage("分组管理");
	}

	protected function _update($name = 'Customer') {
		$id = $_POST['id'];
		$model = D("Customer");
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> letter = get_letter($model -> name);
        $customer =Array (
            ['name'] => $_POST['name'],
            ['short'] => $_POST['short'],
            ['biz_license'] =>$_POST['biz_license'],
            ['contact'] =>$_POST['contact'],
            ['address'] => $_POST['address'],
            ['email'] => $_POST['email'],
            ['office_tel'] => $_POST['office_tel'],
            ['mobile_tel'] =>$_POST['mobile_tel'],
            ['fax'] =>$_POST['fax'],
            ['remark'] => $_POST['remark'],
);
		// 更新数据
		$list = $model -> save($customer);
        if($_POST['notation']){
            $customer_notation=array(
                'customer_id'=>$_POST['id'],
                'user_id'=>$_POST['user_id'],
                'user_name'=>$_POST['user_name'],
                'notation'=>$_POST['notation'],
                'create_time'=>time(),
            );
            M('customer_notation') -> add($customer_notation);
        }
		if (false !== $list) {
			//成功提示
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('编辑成功!');
		} else {
			//错误提示
			$this -> error('编辑失败!');
		}
	}


	protected function _assign_tag_list() {
		$model = D("SystemTag");
		$tag_list = $model -> get_tag_list('id,name');
		$this -> assign("tag_list", $tag_list);
	}
	public function opus($id){
		$list = M('Opus')->where(array('customer_id'=>$id,'is_del'=>0))->select();
        $this -> assign("list", $list);
		$this->display();
	}
	public function opusadd(){
		$this->display();
	}
	public function opusave(){
		$data = array(
			'name'=>$_POST['name'],
			'hjname'=>$_POST['hjname'],
			'sc'=>$_POST['sc'],
			'customer_id'=>$_POST['customer_id'],
			'customer_name'=>$_POST['customer_name'],
			);
		M('Opus')->add($data);
		$this -> assign('jumpUrl', get_return_url());
		$this -> success('增加成功！');

	}
	public function opusread($id){
		$vo = M('Opus')->where(array('id'=>$id))->select();
        $this -> assign("vo", $vo);
		$this->display();	
	}
	public function opusedit($id){
		$vo = M('Opus')->where(array('id'=>$id))->find();
        $this -> assign("vo", $vo);
		$this->display();	
	}
	public function opususave(){
		$data = array(
			'name'=>$_POST['name'],
			'hjname'=>$_POST['hjname'],
			'sc'=>$_POST['sc'],
			'customer_id'=>$_POST['customer_id'],
			'customer_name'=>$_POST['customer_name'],
			);
		M('Opus')->where(array('id'=>$_POST['id']))->save($data);
		$this -> assign('jumpUrl', get_return_url());
		$this -> success('编辑成功！');
	}

	function opusdel($id) {
		$count = $this -> _del($id, Opus, true);
		if ($count) {
			$model = D("SystemTag");
			$result = $model -> del_data_by_row($id);
		}

		if ($count !== false) {//保存成功
			$this -> assign('jumpUrl', get_return_url());
			$this -> success("成功删除{$count}条!");
		} else {
			//失败提示
			$this -> error('删除失败!');
		}
	}
}
?>