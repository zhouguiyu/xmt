<?php
  $where['is_del']='0';
  $task_list = M("Task") ->where($where)->select();
  $data = array();
foreach ($task_list as $key => $value) {
  $add_time = date('Y-m-d',$value['create_time']);
  $finish_time = strtotime($value['expected_time']);
  $finish_time=date('Y-m-d',$finish_time);
  $today = date('Y-m-d',time());
  //完成度
  $finish_rate = M("Task_log") ->where(array('task_id'=>$value['id']))->getField('finish_rate');
  if($today<$finish_time){
     $class= "gantt-block";
   }else if($today>$finish_time){
        if($finish_rate==100){
          $class= "important";
         }else{
           $class= "urgent";
         }
   }
 
  $data[] = array(
  'label' => $value['name'],
  'start' => $add_time, 
  'end'   => $finish_time,
  'class' => $class,
  'finish_rate'=>$finish_rate,
  );
}
?>