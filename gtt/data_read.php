<?php
 if($id){
      $where['id'] =$id;
  }
  $task_list = M("Task") ->where($where)->select();
  $data = array();
foreach ($task_list as $key => $value) {
  $add_time = date($value['start_time']);
  $finish_time = strtotime($value['expected_time']);
  $finish_time = date('Y-m-d',$finish_time);

  $jd = M("Task_jd") ->where(array('tid'=>$id))->select();

  foreach ($jd as $key => $value) {
    if($key!=count($jd)-1){
       $expected_time_jd[]= $value['expected_time_jd'];
    }
  }
 $class ="moren";
  $data[$key] = array(
  'label' => $task_list[0]['name'],
  'start' => $add_time, 
  'end'   => $finish_time,
  'class' => $class,
  'finish_rate'=>$finish_rate,
  'expected_time_jd'=>$expected_time_jd,
  'jd'=>$jd,
  );
}
?>