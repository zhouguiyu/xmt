<?php
if($id){
require('lib/gantti_read.php'); 
require('data_read.php'); 
$cellwidth = 20;

}else{
require('lib/gantti.php'); 
require('data.php'); 
$cellwidth = 20;

}

date_default_timezone_set('UTC');
setlocale(LC_ALL, 'en_US');

$gantti = new Gantti($data, array(
  'title'      => '任务甘特图',
  'cellwidth'  => $cellwidth,
  'cellheight' => 35,
  'today'      => true
));

?>

<!DOCTYPE html>
<html lang="en">
<head>  
  <title>Mahatma Gantti – A simple PHP Gantt Class</title>
  <meta charset="utf-8" />

  <link rel="stylesheet" href="./gtt/styles/css/screen.css" />
  <link rel="stylesheet" href="./gtt/styles/css/gantti.css" />
</head>
<body>
	<a href="index.php?m=&c=Task&a=folder&fid=all" class="back">返回</a>
	<input type="hidden" id="id" value="<?php echo $id; ?>"> 		
	<?php echo $gantti ?>
	 
	<div class="content">
		 <?php if($id){?>
<!-- 		 <div style="float:left;margin-left:100px;">详情：</div> <div class="con_jd">查看详情请点击相对应的颜色条</div>
 -->		 <?php }?>
			<div class="fr rc">			
			<?php if(!$id){?>
			<div><p class="p1"></p><p>进行中</p></div>
			<div><p class="p2"></p><p>完成</p></div>
			<div><p class="p3"></p><p>延期</p></div>			
			<div><span style="color:red;">*</span>数字显示为任务时长（天）</div>
			<div>浅色显示为任务进度</div>
          <?php } else{?>
          	 <div><p class="p2"></p><p>已完成内容</p></div>
			 <div><p class="p1"></p><p>计划内容</p></div>
             <div><p class="p4"></p><p>白色未执行</p></div>	
           <?php }?>
		</div>
    </div>
</body>

</html>



